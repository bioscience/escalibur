#!/bin/bash
# To run these test sets, you have to run these scripts from escalibur main directory and change the parameter setting appropriately.
mkdir -p Data
mkdir Data
# Copy SRR3452139, SRR3441401 and SRR3441123 for CB4854, and SRR3440952, SRR3441150, SRR3441428, SRR3441550, SRR3441658 and SRR3452182 for CB4856 to Data directory.
# Rename them according to inputFile.txt appended here, in which SM1 represents CB4856 and SM2 CB4854.
python3 scripts/inputArgMaker.v.1.0.6.py -o inputFile.txt -p -d Data/ -ph 33 -ml 60 -pl ILLUMINA -lb LIB1 -sm SM1
mkdir -p Ref
zcat ../DataCele/c_elegans.PRJNA13758.WS276.genomic.fa.gz > Ref/cele.fna
java -Dconfig.file=workflow-runtime.local.config -jar cromwell-50.jar run workflow-mapping.wdl -i workflow-mapping.json -o workflow-mapping.outputs.json > out.mapping 2> err.mapping
java -Dconfig.file=workflow-runtime.local.config -jar cromwell-50.jar run workflow-cleaning.wdl -i workflow-cleaning.json -o workflow-cleaning.outputs.json > out.cleaning 2> err.cleaning
# Variant calling is independent i.e. data is calibrated
java -Dconfig.file=workflow-runtime.local.config -jar cromwell-50.jar run workflow-variants.wdl -i workflow-variants.json -o workflow-variants.outputs.json > out.variants 2> err.variants
mv outputVariants outputVariantsIndependent
# Change settings to fast i.e. do not calibrate data
java -Dconfig.file=workflow-runtime.local.config -jar cromwell-50.jar run workflow-variants.wdl -i workflow-variants.json -o workflow-variants.outputs.json > out.variants 2> err.variants
mv outputVariants outputVariantsFast

##################################################################################
rm -f outputVariantsIndependent/*.tbi outputVariantsFast/*.tbi
bgzip -f outputVariantsIndependent/full_genotype_output.vcf
tabix -p vcf outputVariantsIndependent/full_genotype_output.vcf.gz
bgzip -f outputVariantsFast/full_genotype_output.vcf
tabix -p vcf outputVariantsFast/full_genotype_output.vcf.gz

wget https://storage.googleapis.com/elegansvariation.org/releases/20210121/variation/WI.20210121.soft-filter.vcf.gz
bcftools view -s CB4854 WI.20210121.soft-filter.vcf.gz > celeCb4854.soft.vcf
bgzip -f celeCb4854.soft.vcf
tabix -p vcf celeCb4854.soft.vcf.gz
rm celeCb4854.homo.*.tbi
bcftools filter -i 'GT="1|1"|GT="1/1"' celeCb4854.soft.vcf.gz > celeCb4854.homo.vcf
bgzip -f celeCb4854.homo.vcf
tabix -p vcf celeCb4854.homo.vcf.gz

bcftools view -s CB4856 WI.20210121.soft-filter.vcf.gz > celeCb4856.soft.vcf
bgzip -f celeCb4856.soft.vcf
tabix -p vcf celeCb4856.soft.vcf.gz
rm celeCb4856.homo.*.tbi
bcftools filter -i 'GT="1|1"|GT="1/1"' celeCb4856.soft.vcf.gz > celeCb4856.homo.vcf
bgzip -f celeCb4856.homo.vcf
tabix -p vcf celeCb4856.homo.vcf.gz

##################################################################################
## CB4854
bcftools view -s SM2 outputVariantsIndependent/full_genotype_output.vcf.gz > outputVariantsIndependent/celeCb4854.tmp.vcf
bcftools filter -i 'GT="1|1"|GT="1/1"' outputVariantsIndependent/celeCb4854.tmp.vcf | bcftools view  -i  'QUAL>30 && INFO/QD>20 && INFO/SOR<5 && INFO/FS<100' | bcftools view -i 'MIN(FMT/DP)>5' > outputVariantsIndependent/celeCb4854.homo.vcf
bgzip -f outputVariantsIndependent/celeCb4854.homo.vcf
tabix -p vcf outputVariantsIndependent/celeCb4854.homo.vcf.gz
bcftools view -s SM2 outputVariantsFast/full_genotype_output.vcf.gz > outputVariantsFast/celeCb4854.tmp.vcf
bcftools filter -i 'GT="1|1"|GT="1/1"' outputVariantsFast/celeCb4854.tmp.vcf | bcftools view  -i  'QUAL>30 && INFO/QD>20 && INFO/SOR<5 && INFO/FS<100' | bcftools view -i 'MIN(FMT/DP)>5' > outputVariantsFast/celeCb4854.homo.vcf
bgzip -f outputVariantsFast/celeCb4854.homo.vcf
tabix -p vcf outputVariantsFast/celeCb4854.homo.vcf.gz

vcftools --gzvcf celeCb4854.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > celeCb4854.homo.indels.vcf
vcftools --gzvcf outputVariantsFast/celeCb4854.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > outputVariantsFast/celeCb4854.homo.indels.vcf
vcftools --gzvcf outputVariantsIndependent/celeCb4854.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > outputVariantsIndependent/celeCb4854.homo.indels.vcf
bgzip -f celeCb4854.homo.indels.vcf
bgzip -f outputVariantsIndependent/celeCb4854.homo.indels.vcf
bgzip -f outputVariantsFast/celeCb4854.homo.indels.vcf
tabix -p vcf celeCb4854.homo.indels.vcf.gz
tabix -p vcf outputVariantsIndependent/celeCb4854.homo.indels.vcf.gz
tabix -p vcf outputVariantsFast/celeCb4854.homo.indels.vcf.gz
vcf-compare outputVariantsFast/celeCb4854.homo.indels.vcf.gz celeCb4854.homo.indels.vcf.gz
vcf-compare outputVariantsIndependent/celeCb4854.homo.indels.vcf.gz celeCb4854.homo.indels.vcf.gz
vcf-compare outputVariantsFast/celeCb4854.homo.indels.vcf.gz outputVariantsIndependent/celeCb4854.homo.indels.vcf.gz

vcftools --gzvcf celeCb4854.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > celeCb4854.homo.snps.vcf
vcftools --gzvcf outputVariantsFast/celeCb4854.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > outputVariantsFast/celeCb4854.homo.snps.vcf
vcftools --gzvcf outputVariantsIndependent/celeCb4854.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > outputVariantsIndependent/celeCb4854.homo.snps.vcf
bgzip -f celeCb4854.homo.snps.vcf
bgzip -f outputVariantsIndependent/celeCb4854.homo.snps.vcf
bgzip -f outputVariantsFast/celeCb4854.homo.snps.vcf
tabix -p vcf celeCb4854.homo.snps.vcf.gz
tabix -p vcf outputVariantsIndependent/celeCb4854.homo.snps.vcf.gz
tabix -p vcf outputVariantsFast/celeCb4854.homo.snps.vcf.gz
vcf-compare outputVariantsFast/celeCb4854.homo.snps.vcf.gz celeCb4854.homo.snps.vcf.gz
vcf-compare outputVariantsIndependent/celeCb4854.homo.snps.vcf.gz celeCb4854.homo.snps.vcf.gz
vcf-compare outputVariantsFast/celeCb4854.homo.snps.vcf.gz outputVariantsIndependent/celeCb4854.homo.snps.vcf.gz

##################################################################################
## CB4856
bcftools view -s SM1 outputVariantsIndependent/full_genotype_output.vcf.gz > outputVariantsIndependent/celeCb4856.tmp.vcf
bcftools filter -i 'GT="1|1"|GT="1/1"' outputVariantsIndependent/celeCb4856.tmp.vcf | bcftools view  -i  'QUAL>30 && INFO/QD>20 && INFO/SOR<5 && INFO/FS<100' | bcftools view -i 'MIN(FMT/DP)>5' > outputVariantsIndependent/celeCb4856.homo.vcf
bgzip -f outputVariantsIndependent/celeCb4856.homo.vcf
tabix -p vcf outputVariantsIndependent/celeCb4856.homo.vcf.gz
bcftools view -s SM1 outputVariantsFast/full_genotype_output.vcf.gz > outputVariantsFast/celeCb4856.tmp.vcf
bcftools filter -i 'GT="1|1"|GT="1/1"' outputVariantsFast/celeCb4856.tmp.vcf | bcftools view  -i  'QUAL>30 && INFO/QD>20 && INFO/SOR<5 && INFO/FS<100' | bcftools view -i 'MIN(FMT/DP)>5' > outputVariantsFast/celeCb4856.homo.vcf
bgzip -f outputVariantsFast/celeCb4856.homo.vcf
tabix -p vcf outputVariantsFast/celeCb4856.homo.vcf.gz

vcftools --gzvcf celeCb4856.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > celeCb4856.homo.indels.vcf
vcftools --gzvcf outputVariantsFast/celeCb4856.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > outputVariantsFast/celeCb4856.homo.indels.vcf
vcftools --gzvcf outputVariantsIndependent/celeCb4856.homo.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout > outputVariantsIndependent/celeCb4856.homo.indels.vcf
bgzip -f celeCb4856.homo.indels.vcf
bgzip -f outputVariantsIndependent/celeCb4856.homo.indels.vcf
bgzip -f outputVariantsFast/celeCb4856.homo.indels.vcf
tabix -p vcf celeCb4856.homo.indels.vcf.gz
tabix -p vcf outputVariantsIndependent/celeCb4856.homo.indels.vcf.gz
tabix -p vcf outputVariantsFast/celeCb4856.homo.indels.vcf.gz
vcf-compare outputVariantsFast/celeCb4856.homo.indels.vcf.gz celeCb4856.homo.indels.vcf.gz
vcf-compare outputVariantsIndependent/celeCb4856.homo.indels.vcf.gz celeCb4856.homo.indels.vcf.gz
vcf-compare outputVariantsFast/celeCb4856.homo.indels.vcf.gz outputVariantsIndependent/celeCb4856.homo.indels.vcf.gz

vcftools --gzvcf celeCb4856.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > celeCb4856.homo.snps.vcf
vcftools --gzvcf outputVariantsFast/celeCb4856.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > outputVariantsFast/celeCb4856.homo.snps.vcf
vcftools --gzvcf outputVariantsIndependent/celeCb4856.homo.vcf.gz --remove-indels --recode --recode-INFO-all --stdout > outputVariantsIndependent/celeCb4856.homo.snps.vcf
bgzip -f celeCb4856.homo.snps.vcf
bgzip -f outputVariantsIndependent/celeCb4856.homo.snps.vcf
bgzip -f outputVariantsFast/celeCb4856.homo.snps.vcf
tabix -p vcf celeCb4856.homo.snps.vcf.gz
tabix -p vcf outputVariantsIndependent/celeCb4856.homo.snps.vcf.gz
tabix -p vcf outputVariantsFast/celeCb4856.homo.snps.vcf.gz
vcf-compare outputVariantsFast/celeCb4856.homo.snps.vcf.gz celeCb4856.homo.snps.vcf.gz
vcf-compare outputVariantsIndependent/celeCb4856.homo.snps.vcf.gz celeCb4856.homo.snps.vcf.gz
vcf-compare outputVariantsFast/celeCb4856.homo.snps.vcf.gz outputVariantsIndependent/celeCb4856.homo.snps.vcf.gz
