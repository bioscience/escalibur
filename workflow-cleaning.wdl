#############################################################################
#############################################################################
##
## Escalibur - Population Genomic Analysis Pipeline
## overcomes the difficulties in variant calling and is able to explore key
## aspects centering the population genetics of helminths, including defining
## population structures, estimating reproductive modes and identifying loci
## under local adaptation.
##
## Software usage: Java
## Input: Fastq files
##
##############################################################################
##############################################################################


############################### Import sub workflows #########################################
import "./sub_workflows/index_database_workflow.wdl" as indexingSubWorkflow
import "./sub_workflows/clean_bams_workflow.wdl" as cleanBamsSubWorkflow
import "./sub_workflows/create_cleaned_bams_workflow.wdl" as createCleanedBamsSubWorkflow

######################### Import tasks #######################################################
import "./tasks/cleanBams_task.wdl" as cleanBams
import "./tasks/readFiles_task.wdl" as readFiles
import "./tasks/index_task.wdl" as indexTask

workflow cleaning_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "babak.shaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
		author: "Liina Kinkar"
		email: "liina.kinkar@unimelb.edu.au"
		author: "Pasi Korhonen"
		email: "pasi.korhonen@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}

	String? singularityContainerPath
	String? singularityBindPath
	String? singularityContainerString = "singularity run -B " + singularityBindPath + " " + singularityContainerPath + ""

	# input for fastq sample files to be processed
	File inputContaminantFile # tsv of putative contaminant genomes and their expected sample ids
        File extractSamplesByContaminants
        File extractRefsBySample
        File returnContaminantRefs
        File compareMappings
	#Int nSamples = length(arrayOfInputSamples)

	call readFiles.extractContaminantRefs {
		input:
		    singularityContainerString = singularityContainerString,
		    contaminantFile = inputContaminantFile,
                    script = returnContaminantRefs
	}

        scatter (ref in extractContaminantRefs.references) {
                call indexTask.indexing_bwa_task {
	                input:
	                    singularityContainerString = singularityContainerString,
	                    RefFastaFile = ref
                }
        }

	call readFiles.samplesByContaminants {
		input:
		    singularityContainerString = singularityContainerString,
		    contaminantFile = inputContaminantFile,
                    contaminantRefIndices = indexing_bwa_task.bwaIndexFiles,
                    script = extractSamplesByContaminants
	}

        scatter (items in samplesByContaminants.sampleIdsAndBamFiles) {
	        call readFiles.refsBySample {
		        input:
		            singularityContainerString = singularityContainerString,
		            contaminantFile = inputContaminantFile,
                            contaminantRefIndices = indexing_bwa_task.bwaIndexFiles,
                            sampleId = items[0],
                            script = extractRefsBySample
	        }
        }

	scatter (pair in zip(samplesByContaminants.sampleIdsAndBamFiles, refsBySample.sampleContaminantRefs)) {
		call cleanBamsSubWorkflow.clean_bams_workflow {
			input:
			    singularityContainerString = singularityContainerString,
                            sampleId = pair.left[0],
                	    sampleBamFile = pair.left[1],
			    contaminantRefs = pair.right,
                            script = compareMappings
		}

		call createCleanedBamsSubWorkflow.create_cleaned_bams_workflow {
			input:
			    singularityContainerString = singularityContainerString,
                            sampleId = pair.left[0],
                	    sampleBamFile = pair.left[1],
			    contaminantReadIds = clean_bams_workflow.contaminantReadIds
		}
		#call cleanBams.createCleanedBams_task {
		#	input:
		#	    singularityContainerString = singularityContainerString,
                #            sampleId = pair.left[0],
                #	    sampleBamFile = pair.left[1],
		#	    contaminantReadIds = clean_bams_workflow.contaminantReadIds
		#}
	}

	#scatter (pair in zip(samplesByContaminants.sampleIdsAndBamFiles, clean_bams_workflow.contaminantReadIds)) {
	#	call cleanBams.createCleanedBams_task {
	#		input:
	#		    singularityContainerString = singularityContainerString,
        #                   sampleId = pair.left[0],
        #        	    sampleBamFile = pair.left[1],
	#		    contaminantReadIds = pair.right
	#	}
	#}

	############  capture data filtering output ###############
        output {
	    #Array[Array[File]] contaminantReadIds = clean_bams_workflow.contaminantReadIds
	    Array[File] contaminantReadIds = clean_bams_workflow.contaminantReadIds
      	    Array[File] cleanedBams = create_cleaned_bams_workflow.cleanedBam
      	    Array[File] cleanedBais = create_cleaned_bams_workflow.cleanedBai
      	    Array[File] cleanedBamStats = create_cleaned_bams_workflow.cleanedBamStats
      	    #Array[File] cleanedBams = createCleanedBams_task.cleanedBam
      	    #Array[File] cleanedBais = createCleanedBams_task.cleanedBai
      	    #Array[File] cleanedBamStats = createCleanedBams_task.cleanedBamStats
	}
}
