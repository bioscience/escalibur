#############################################################################
#############################################################################
##
## Escalibur - Population Genomic Analysis Pipeline
## overcomes the difficulties in variant calling and is able to explore key
## aspects centering the population genetics of helminths, including defining
## population structures, estimating reproductive modes and identifying loci
## under local adaptation.
##
## Software usage: Java
## Input: Fastq files
##
##############################################################################
##############################################################################


############################### Import sub workflows #########################################
import "./sub_workflows/index_database_workflow.wdl" as indexingSubWorkflow
import "./sub_workflows/pe_filtering_workflow.wdl" as peFilteringSubworkflow
import "./sub_workflows/mapping_workflow.wdl" as mappingSubWorkflow
import "./sub_workflows/processHistStats_workflow.wdl" as processHistogramSubWorkflow
import "./sub_workflows/qc_mDup_samIdx_workflow.wdl" as qcMdupSubWorkflow

######################### Import tasks #######################################################
import "./tasks/read_report_task.wdl" as selectBestDb
import "./tasks/create_best_db_map.wdl" as bestDbMap
import "./tasks/filterBestRefGenomeBamFiles_task.wdl" as filterBestDb_task
import "./tasks/readFiles_task.wdl" as readFiles


workflow mapping_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "babak.shaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}
	String RscriptPath
	# TODO: Resolve this path vs file issue
	String ReadSummaryRscript
	#File ReadSummaryRscript
	File ReadSummaryRscriptPath = RscriptPath + ReadSummaryRscript
	#File perlScriptPath
	#File findDbScriptPath = perlScriptPath +
        File truseq_pe_adapter
        File truseq_se_adapter

	File findDbScript
	File sortBySampleId
	Int ploidy
        String readQc
	String trimmomatic
	String picard
	String? singularityContainerPath
	String? singularityBindPath
	String? singularityContainerString = "singularity run -B " + singularityBindPath + " " + singularityContainerPath + ""

	# input for fastq sample files to be processed
	File inputSampleFile # tsv of input files
	Array[Array[String]] arrayOfInputSamples = read_tsv(inputSampleFile)
	# Get number of samples
	Int nSamples = length(arrayOfInputSamples)
	Boolean pipeToViewBoolean
        String samtoolsParameters

	# input for reference fasta files to be processed
	File inputReferenceFile
	Array[Array[File]] arrayOfReferenceFiles = read_tsv(inputReferenceFile)

	#call subworkflow indexing
	call indexingSubWorkflow.index_sub_workflow {
		input:
		    singularityContainerString = singularityContainerString,
		    referenceArray = arrayOfReferenceFiles
	}

	#scatter to start data filtering and alignment
	scatter (sample in arrayOfInputSamples) {
	    ## at some stage will have to add se sub workflow flag?
	    call peFilteringSubworkflow.pe_filtering_workflow {
			input :
				singularityContainerString = singularityContainerString,
				outputPrefix = sample[0],
				forwardReads = sample[10],
				reverseReads = sample[11],
				EndType = sample[1],
				trimmomatic = trimmomatic,
				Phred = sample[3],
				minLength = sample[2],
				PhredQuality = sample[5],
                                readQc = readQc,
                                adapter = truseq_pe_adapter
	    }
            String? forwardReads = if readQc=="yes" then pe_filtering_workflow.forwardTrimmedPEReads else sample[10]
            String? reverseReads = if readQc=="yes" then pe_filtering_workflow.reverseTrimmedPEReads else sample[11]

	    ## call mapping workflow
	    #forward/reverse read should come from filtering step
	    call mappingSubWorkflow.sample_mapping  {
			input :
				singularityContainerString = singularityContainerString,
				outputPrefix = sample[0],
            			forwardReads = forwardReads,
            			reverseReads = reverseReads,
				platform = sample[4],
				ReadGroupHeader = "\"@RG\\tID:${sample[7]}\\tPL:${sample[4]}\\tPU:${sample[9]}\\tLB:${sample[6]}\\tSM:${sample[8]}\"",
				pipeToViewBoolean = pipeToViewBoolean,
                                samtoolsParameters = samtoolsParameters,
				refDbIndexFiles = index_sub_workflow.bwaIndexFiles,
                                EndType = sample[1]
	    }
	}

	############# Read Reports Sub workflow ####################
	call processHistogramSubWorkflow.processStats_workflow {
		input :
			singularityContainerString = singularityContainerString,
                        singularityBindPath = singularityBindPath,
                        singularityContainerPath = singularityContainerPath,
 			refDbIndexFiles = index_sub_workflow.bwaIndexFiles,
			pre_formatHistArray = pre_formatHistArray,
			post_formatHistArray = post_formatHistArray,
			outputGrepStats = sample_mapping.outputGrepStats,
			ReadSummaryRscript = ReadSummaryRscriptPath,
			sortedBamStats = sortedBamStats
	}

	call selectBestDb.findBestRefDBFromReport_task {
		input :
			singularityContainerString = singularityContainerString,
			refDBstats = processStats_workflow.refDBstats,
			findDbScript = findDbScript
	}

	scatter (refFastaFile in arrayOfReferenceFiles){
        	call bestDbMap.create_best_db_map_task {
            	input :
			singularityContainerString = singularityContainerString,
                	bestDb = findBestRefDBFromReport_task.bestDB,
			picardString = picard,
                	referenceFastaFile = refFastaFile[1]
        	}
	}

	################# Set best Database input ##############################
	Map[String, File] bestDbMap = {"refFasta": select_first(create_best_db_map_task.referenceFastaFileOutput), "refFastaIndex": select_first(create_best_db_map_task.referenceFastaIndex), "refDict": select_first(create_best_db_map_task.referenceSeqDict) }

	################# QC mark duplicates subworkflow ########################
	# Flatten returned sorted bam file array
	# Will later add option to run fast, joint or non-joint implementation
	Array[File?] flatBamArray = flatten(sample_mapping.sortedBamFilesArray)
        
	call filterBestDb_task.filterBestRefGenomeBamFiles_task as filterBamFile_tsk {
		input :
			bestDb = findBestRefDBFromReport_task.bestDB,
			unfilteredBamFiles = flatBamArray
        
	}
        
	call readFiles.sortFilesBySample {
		input:
			singularityContainerString = singularityContainerString,
			readsFile = inputSampleFile,
			#bamFiles = flatBamArray,
			bamFiles = filterBamFile_tsk.filteredBamFiles,
                        script = sortBySampleId
	}

	scatter (pair in zip(sortFilesBySample.sampleIds, sortFilesBySample.sampleSortedFiles)) {
		call qcMdupSubWorkflow.qc_mDup_sidx_workflow {
			input :
				singularityContainerString = singularityContainerString,
				picardString = picard,
				sortedBams = pair.right,
                                base = pair.left,
				picardString = picard,
				bestDbMap = bestDbMap,
				bestDb = findBestRefDBFromReport_task.bestDB,
				ploidy = ploidy
		}
	}


	############  capture data filtering output ###############
	# capture data filtering output
        output {
		################ Capture indexing output ######################################
		#Array[Map[String, File]] bwaIndexFiles = index_sub_workflow.bwaIndexFiles
		#Array[File] multiqcArray = pe_filtering_workflow.multiqcArray
		Array[Array[File]] pre_formatHistArray = pe_filtering_workflow.pre_formatHistArray
		Array[Array[File]] post_formatHistArray = pe_filtering_workflow.post_formatHistArray
		Array[File?] forwardTrimmedPEReads = pe_filtering_workflow.forwardTrimmedPEReads
    		Array[File?] reverseTrimmedPEReads = pe_filtering_workflow.reverseTrimmedPEReads
		Array[Array[File]] sortedBamStats = sample_mapping.sortedBamStats

		################## ReadSummary output #####################
		Array[File] covStats = processStats_workflow.covStats
		Array[File] isStats = processStats_workflow.isStats
		Array[File] snStats = processStats_workflow.snStats
		Array[File] refDBstats = processStats_workflow.refDBstats

		################## best db #############################
		File bestRef = findBestRefDBFromReport_task.bestRef
                #File bestRef = bestDbMap["refFasta"]
                #File bestIdx = bestDbMap["refFastaIndex"]
                #File bestGen = select_first(create_best_db_map_task.referenceFastaFileOutput)
                #Map[String, File] bestDbMap = bestDbMap

		################## QC MarkDup - Fast Implementation ############
		#### Possibly change into map instead of 3 arrays #############
		Array[File] mDupSortBamArray = qc_mDup_sidx_workflow.mDupSortedBam
		Array[File] mDupSortBamIndArray = qc_mDup_sidx_workflow.mDupSortedBamIndex
	}
}
