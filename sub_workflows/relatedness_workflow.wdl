## workflow replicating step2-relatedness from the perl version.
## author: Noel Faux, Pasi Korhonen
## 03-March-2020
## Melbourne Data Analyitcs


import "./tasks/vcf_bgzip_task.wdl" as vcfBgZipTask
import "./tasks/perl_task.wdl" as perlTask

workflow relatedness {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "bshaban@unimelb.edu.au"
		author: "Pasi Korhonen"
		email: "pasi.korhonen@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}
	File inputVCFGZfile
	File relatedFile
	File filteredRelateFile
	String perlScript = "./perl_scripts/filter_relatedness.pl"

	call vcfBgZipTask.vcf_bgzip_task {
		input:
			VCFinputFile = nputVCFGZfile
			withOut = true
			OutputFile = relatedFile

			gzvcf = true
			relatedness2 = true

			withStdOut = false
			chromosomes = false
			meanDp = false
			missing = false
			alleles = false
			singletons = false
			excludePos = false
			minQ = false
			convertPlink = false
			recode = false
			recodeInfoAll = false
			removeFilterAll = false
			bgzip = false
	}

	call perlTask.perl_task {
		input:
			withFile = true
			perlFile = ${perlScript}
			cmdlnArgs = "-rf ${relatedFile} -o ${filteredRelateFile}"
	}
}
