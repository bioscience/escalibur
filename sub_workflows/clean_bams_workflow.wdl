#####################################################################################
# QC - mDuplicate work flow
#
# 06/05/2020
#
# QC - "fast" implementation of the pipeline
#    - Consists of calling mark duplicates -> samtools index -> haplotype caller
#
# Runs per sample
#
#
# Input: 	Sorted bam files from sample mapping
# Changes: 	Possibly change output to Map
######################################################################################
####
# Workflow representing the intial qc workflow unit before variant calling
# gatk Mark Duplicates -> samtools index
# author: Noel Faux
# Integration: Bobbie Shaban
####
################### Import tasks ###################################
import "./tasks/cleanBams_task.wdl" as cleanBamsTask

workflow clean_bams_workflow {
    meta {
        author: "Liina Kinkar"
        email: "liina.kinkar@unimelb.edu.au"
        author: "Pasi Korhonen"
        email: "pasi.korhonen@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    String singularityContainerString
    File sampleBamFile
    Map[String, File] contaminantRefs
    #Array[Map[String, File]?] contaminantRefs
    String sampleId
    File script

    #scatter (ref in contaminantRefs) {
	call cleanBamsTask.cleanBams_task {
	    input:
	        singularityContainerString = singularityContainerString,
               	sampleBamFile = sampleBamFile,
		#contaminantRef = ref,
		contaminantRef = contaminantRefs,
                sampleId = sampleId,
                script = script
	}
    #}

    #call cleanBamsTask.cleanBams_task as clean_task {
    #    input :
    #        sampleBamFile = sampleBamFile
    #        contaminantRefs = contaminantRefs,
    #        contaminantRefIndexes = contaminantIndexFiles,
    #        sampleId = sampleId,
    #	    singularityContainerString = singularityContainerString,
    #}

    output {
        File contaminantReadIds = cleanBams_task.contaminantReadIds
        #Array[File] contaminantReadIds = cleanBams_task.contaminantReadIds
    }
}
