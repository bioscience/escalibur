####################################################################################################
#
# Independant/individual variant SNP and INDEL calling for all samples
# In the perl version this covers the Calibration.pm non-joint-recalibration
# non-fast workflow.
####################################################################################################

import "./sub_workflows/qc_mDup_samIdx_hapcal_workflow.wdl" as dup_idx_hcall_wf
import "./sub_workflows/selectFilterZip_workflow.wdl" as sv_vf_tab_wf
import "./sub_workflows/base_recalibration_workflow.wdl" as baseRecalibration_workflow

workflow independant_variant_calling_wf {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        author: "Pasi Korhonen"
        email: "pasi.korhonen@unimelb.edu.au"
        description: "Workflow encapsulating the pipeline, \n\nMark Duplicates --> Sam Index --> Haplotype Calling --> Select Variants (SNP and INDEL) --> Variant Filter --> Base Recalibration --> Haplotype Calling"
    }
    parameter_meta {
        # Inputs:
        refGenomeDbMap: "itype:Map[String, File]: Containin the reference database auxiliary index files"
        sample: "itype:File: Individual genomic sample to call varaients for <file ext or type?>"
        ploidy: "itype:Int: indicating ploidy, e.g. diploid = 2"
        SNP_filt_exp: "itype:String:SNP filter expression for gatk SelectVariants tool"
        INDEL_filt_exp: "itype:String: SNP filter expression for gatk SelectVariants tool"
        # Outputs:
        Initial_RecaibratedSample: "otype:File: ..."
        AfterApply_RecaibratedSample: "otype:File: ..."
        RecalHapCHOutFile: "otype:File: ...base_recalibration_wf.FinalHapCallOutFile"
    }

    Int ploidy
    Map[String, File] refGenomeDbMap # bestDbMap
    Map[String, File] sampleMap # bam, bam index, gvcf
    String bestDb
    String SNP_filt_exp
    String INDEL_filt_exp
    String picardString
    String singularityContainerString
    String singularityContainerPath

    #SNP
    call sv_vf_tab_wf.selectFilterZip_workflow as snp_svf_tab_wf {
        input :
            refGenomeDbMap = refGenomeDbMap,
            genoTypeOutput = sampleMap["gvcf"],
            selectType = "SNP",
            filterExpression = SNP_filt_exp,
	    singularityContainerString = singularityContainerString,
            filterName = "my_snp_filter"
    }

    #INDEL
    call sv_vf_tab_wf.selectFilterZip_workflow as indel_svf_tab_wf {
        input :
            refGenomeDbMap = refGenomeDbMap,
            genoTypeOutput = sampleMap["gvcf"],
            selectType = "INDEL",
            filterExpression = INDEL_filt_exp,
	    singularityContainerString = singularityContainerString,
            filterName = "my_indel_filter"
    }

    # base recal -> apply -> idx -> base cal
    call baseRecalibration_workflow.baseRecalibration_workflow as base_recalibration_wf {
        input :
            refGenomeDbMap = refGenomeDbMap,
            bestDb = bestDb,
            sample = sampleMap["bam"],
            snpSitesMap = {"bgz": snp_svf_tab_wf.bgzipOutput, "index": snp_svf_tab_wf.bgzipTabixOutput},
            indelSitesMap = {"bgz": indel_svf_tab_wf.bgzipOutput, "index": indel_svf_tab_wf.bgzipTabixOutput},
            ploidy = ploidy,
	    singularityContainerString = singularityContainerString,
            picardString = picardString
    }

    output {
        File Initial_RecaibratedSampleTable = "${base_recalibration_wf.Initial_RecaibratedSample_Table_File}"
        File AfterApply_RecaibratedSampleTable = "${base_recalibration_wf.AfterApply_RecaibratedSample_Table_File}"
        File sampleInitialRecal_BSQL_bam_file = "${base_recalibration_wf.sapmleFistRecal_BQSR_Bam_File}"
        File sampleSecondRecal_BSQL_bam_file = "${base_recalibration_wf.sapmleSecondRecal_BQSR_Bam_File}"
        File RecalPlots_File = "${base_recalibration_wf.RecalPlots_File}"
        File RecalCSV_File = "${base_recalibration_wf.RecalCSV_File}"
        File RecalHapCHOutFile = "${base_recalibration_wf.Varcall_After_RecalSample_GVCF_File}"
    }
}
