## workflow replicating step1-recalibration from the perl version.
## author: Noel Faux, Pasi Korhonen
## 04-March-2020
## Melbourne Data Analytics

import "../tasks/gatk_MarkDuplicate_task.wdl" as MarkDuplicateTask
import "../tasks/Index_tasks.wdl" as IndexTasks
import "../tasks/gatk_HaplotypeCaller_task.wdl" as HaplotypeCaller1Task
import "./combineGenotype_workflow.wdl" as CombineGenotypeWorkflow
import "./selectFilterZip_workflow.wdl" as SelectFilterWorkflow
import "./base_recalibration_workflow.wdl" as baseRecalibrationWorkflow
import "../tasks/gatk_AnalyseCovariates_task.wdl" as AnalyseCovarsTask
import "../tasks/writeOutput_task.wdl" as writeOutputTask


String SNPfilterExpression = "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0",
String INDELfilterExpression = "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"


workflow Calibration_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "bshaban@unimelb.edu.au"
		description: “…”
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: …"
		# Outputs:
		Output1: "otype:<TYPE>: ..."
	}
	Boolean jointCallingMode
	File referenceDb
	Array[File] refDbIndexFiles
	Int ploidy

	String resultsDir

	Array[File] samples
	File metrics

	if(jointCallingMode){
		# define the duplicates SNPs in each sample
		scatter(sample in samples){
			call MarkDuplicateTask.gatk_MarkDuplicate_task {
				input:
					Input = sample,
					metrics = metrics,
					Output = "${sample}.markdup.bam"
			}
			call IndexTasks.indexing_sam_task {
				input:
					index = true,
					DbBase = gatk_MarkDuplicate_task.DuplicateOut
			}
			call HaplotypeCallerTask.gatk_HaplotypeCaller_task as gatkHapCaller_T1{
				input:
					emitRefConf = true,
					reference = referenceDb,
					ploidy = ploidy,
					Input = gatk_MarkDuplicate_task.DuplicateOut,
					Output = "${sample}.HC.1st.gvcf.gz"
			}
		} # End of the scatter to define the duplicates

		# Combine select SNPs and INDELs and filter
		call CombineGenotypeWorkflow.combineGenotype_workflow as combinGenotype_T1{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				ploidy = ploidy,
				samples = HaplotypeCaller1Task.gatk_HaplotypeCaller_task.HapOutput,
		}
		call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_SNP1{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				sample = combinGenotype_T1.combineGenotypeOutput,
				selectType = "SNP",
				filter = "snp_filter",
				filterExpression = SNPfilterExpression
		}

		call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_INDEL1{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				sample = combinGenotype_T1.combineGenotypeOutput,
				selectType = "INDEL",
				filter = "indel_filter",
				filterExpression = INDELfilterExpression,
		}

		# From the inital select filter perform first round of calibration across all samples
		scatter(sample in samples) {
			call baseRecalibrationWorkflow.base_recalibration_workflow as baseRecal_T1{
				input :
					sample = sample,
					referenceDb = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					snpStats = selectFilter_SNP1.filteredOut,
					indelStats = selectFilter_INDEL1.filteredOutput,
					ploidy = ploidy
			}
			call HaplotypeCallerTask.gatk_HaplotypeCaller_task as gatkHapCaller_T2{
				input :
					emitRefConf = true,
					reference = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					ploidy = ploidy,
					Input = baseRecal_T1.AfterApply_RecaibratedSample,
					Output = "${sample}.HC.2nd.gvcf.gz"
			}

			call AnalyseCovarsTask.gatk_AnalyzeCovariates_task as AnalyseCovars_R1{
				input :
					beforeReport = baseRecal_T1.Initial_RecaibratedSample,
					afterReport = baseRecal_T1.AfterApply_RecaibratedSample,
					plot = "${sample}.recalQC_1st_plot.pdf"
			}
		}
		# Write all the summary analyse covarsiate plots to disk
		call writeOutputTask.writeOutput_task as writeInitialAnalysis {
			input :
				filesArray = AnalyseCovars_R1.PlotReport
				outdir = "${resultsDir}/covariate_analysis_plots/"
		}


		# Second round joint/combining after recalibration
		call CombineGenotypeWorkflow.combineGenotype_workflow as combinGenotype_T2{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				ploidy = ploidy,
				samples = gatkHapCaller_T2.HapOutput
		}

		call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_SNP2{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				sample = combinGenotype_T2.combineGenotypeOutput,
				selectType = "SNP",
				filter = "snp_filter",
				filterExpression = SNPfilterExpression
		}

		call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_INDEL2{
			input :
				referenceDb = referenceDb,
				refDbIndexFiles = refDbIndexFiles,
				sample = combinGenotype_T2.combineGenotypeOutput,
				selectType = "INDEL",
				filter = "indel_filter",
				filterExpression = INDELfilterExpression,
		}

		# Second round of recalibration
		scatter(sample in samples) {
			call baseRecalibrationWorkflow.base_recalibration_workflow as baseRecal_T2{
				input :
					sample = sample,
					referenceDb = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					snpStats = selectFilter_SNP2.filteredOut,
					indelStats = selectFilter_INDEL2.filteredOutput,
					ploidy = ploidy
			}

			call HaplotypeCallerTask.gatk_HaplotypeCaller_task as gatkHapCaller_T3{
				input :
					emitRefConf = true,
					reference = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					ploidy = ploidy,
					Input = baseRecal_T2.AfterApply_RecaibratedSample,
					Output = "${sample}.HC.2nd.gvcf.gz"
			}

			call AnalyseCovarsTask.gatk_AnalyzeCovariates_task as AnalyseCovars_R2{
				input :
					beforeReport = baseRecal_T2.Initial_RecaibratedSample,
					afterReport = baseRecal_T2.AfterApply_RecaibratedSample,
					plot = "${sample}.recalQC_2nd_plot.pdf"
			}
		}
		# Write all the summary analyse covarsiate plots to disk
		call writeOutputTask.writeOutput_task as writeSecondRndAnalysis {
			input :
				filesArray = AnalyseCovars_R2.PlotReport
				outdir = "${resultsDir}/covariate_analysis_plots/"
		}

		output {
			# TODO: Need to check this is the correct output
			Array[File] RecalibratedCalls = gatkHapCaller_T3.HapOutput
		}
	}

	# Independent varaiant calling
	if(!jointCallingMode){
		scatter(sample in samples){
			call MarkDuplicateTask.gatk_MarkDuplicate_task {
				input:
					Input = sample,
					metrics = metrics,
					Output = "${sample}.markdup.bam"
			}
			call IndexTasks.indexing_sam_task {
				input :
					index = true,
					DbBase = gatk_MarkDuplicate_task.DuplicateOut
			}
			call HaplotypeCallerTask.gatk_HaplotypeCaller_task {
				imput :
					emitRefConf = true,
					reference = referenceDb,
					refDbIndexFiles = [refDbIndexFiles, indexing_sam.samIndexFile]
					ploidy = ploidy,
					Input = gatk_MarkDuplicate_task.DuplicateOut,
					Output = "${sample}.HC.1st.gvcf.gz"

			call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_SNP {
				input :
					referenceDb = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					sample = gatk_HaplotypeCaller_task.HapOutput,
					selectType = "SNP",
				filter = "snp_filter",
				filterExpression = SNPfilterExpression
			}
			call SelectFilterWorkflow.selectFilterZip_workflow as selectFilter_INDEL{
				input :
					referenceDb = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					sample = HaplotypeCallerTask.gatk_HaplotypeCaller_task.HapOutput,
					selectType = "INDEL",
					filter = "indel_filter",
					filterExpression = INDELfilterExpression,
			}

			call baseRecalibrationWorkflow.base_recalibration_workflow {
				input :
					sample = sample,
					referenceDb = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					snpStats = selectFilter_SNP.filteredOut,
					indelStats = selectFilter_INDEL.filteredOutput,
					ploidy = ploidy
			}
			# This call is not in the initial perl pipeline - added it here
			# to keep it consistant with the joint calling workflow
			call HaplotypeCallerTask.gatk_HaplotypeCaller_task{
				input :
					emitRefConf = true,
					reference = referenceDb,
					refDbIndexFiles = refDbIndexFiles,
					ploidy = ploidy,
					Input = base_recalibration_workflow.AfterApply_RecaibratedSample,
					Output = "${sample}.HC.gvcf.gz"
			}
		}
		output {
			Array[File] RecalibratedCalls = bgatk_HaplotypeCaller_task.HapOutput
		}
	}
}
