#####################################################################################
# QC - mDuplicate work flow
#
# 06/05/2020
#
# QC - "fast" implementation of the pipeline
#    - Consists of calling mark duplicates -> samtools index -> haplotype caller
#
# Runs per sample
#
#
# Input: 	Sorted bam files from sample mapping
# Changes: 	Possibly change output to Map
######################################################################################
####
#
# Workflow representing the intial qc workflow unit before variant calling
# gatk Mark Duplicates -> samtools index -> gatk Haplotype Caller
# author: Noel Faux
# Integration: Bobbie Shaban
####
################### Import tasks ###################################
import "./tasks/index_task.wdl" as index_task
import "./tasks/gatk_HaplotypeCaller_task.wdl" as hap_caller_task

workflow qc_sidx_hc_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    Int ploidy
    Map[String, File] bestDbMap
    String bestDb
    File inputBam
    String base = basename(inputBam, ".txt")
    String singularityContainerString
    String picardString

    call index_task.indexing_bam_file as md_task {
        input:
            bamFile = inputBam,
	    singularityContainerString = singularityContainerString
    }

    call hap_caller_task.gatk_HaplotypeCaller_task as hap_task {
        input:
            bestDb = bestDb,
            bestDbMap = bestDbMap,
            base = base,
            inputBam = md_task.bamLinkFile,
	    inputBamIndex = md_task.bamIndexFile,
            emitRefConf = true,
            ploidy = ploidy,
	    picardString = picardString,
	    singularityContainerString = singularityContainerString
    }

    output {
        File mDupSortedBam = inputBam
    	File mDupSortedBamIndex = md_task.bamIndexFile
    	File? haplo_GVCF = hap_task.GVCF
        Map[String, File?] mDupSortedBamAndGVCFMap = {"bam": inputBam, "bamidx": md_task.bamIndexFile, "gvcf": hap_task.GVCF}
    }
}
