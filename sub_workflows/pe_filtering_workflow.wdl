##############################################################
##############################################################
###
###
###  Replicating the Filtering perl module
### author: Noel Faux
### 12/03/2020
###
### Two workflows:
###    - paired end - filtering_workflow_pe
###    - single end - filtering_workflow_se
###
### Desc: Indexes the reference genomes - Paired end and SE flows
### Soft: Trimmomatic, BBMAP
###
### Notes: Trimmomatic currently takes output from reformat
###        not the raw reads
###        Removed the scatter inside sub workflows and moved
###        to the main workflow
###
##############################################################
##############################################################


###NOTE: Import files from the perpsective of the main workflow
import "./tasks/data_Filtering_task.wdl" as filtering_tasks

workflow pe_filtering_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    File forwardReads
    File reverseReads
    File adapter
    String outputPrefix
    String EndType
    String singularityContainerString
    String trimmomatic
    Int minLength
    Int Phred
    Int PhredQuality
    String readQc

    call filtering_tasks.fastqc_task {
	    input:
		singularityContainerString = singularityContainerString,
		forwardReads = forwardReads,
		reverseReads = reverseReads
	}

#	call filtering_tasks.multiqc_task {
#	    input:
#		singularityContainerString = singularityContainerString,
#		fastqcArray = fastqc_task.fastqcArray,
#		outputPrefix = outputPrefix
#	}

    call filtering_tasks.reformat_pe_task {
        input :
	    singularityContainerString = singularityContainerString,
            forwardReads = forwardReads,
            reverseReads = reverseReads,
	    suffix = "pre",
            outputPrefix = outputPrefix
    }

    if (readQc == "yes") {
        call filtering_tasks.trimmomatic_pe_task {
           input :
	       singularityContainerString = singularityContainerString,
               EndType = EndType,
               Phred = Phred,
               PhredQuality = PhredQuality,
               forwardReads = forwardReads,
               reverseReads = reverseReads,
               outputPrefix = outputPrefix,
               #forwardReads = reformat_pe_task.outFqFiles[0],
               #reverseReads = reformat_pe_task.outFqFiles[1],
               minLength  = minLength,
               trimmomatic = trimmomatic,
               adapter = adapter
        }
    }

    String? fReads = if readQc=="yes" then trimmomatic_pe_task.outFwdPaired else forwardReads
    String? rReads = if readQc=="yes" then trimmomatic_pe_task.outRevPaired else reverseReads

    call filtering_tasks.reformat_pe_task as pe_post_reformat {
        input :
            singularityContainerString = singularityContainerString,
            forwardReads = fReads,
            reverseReads = rReads,
            #forwardReads = trimmomatic_pe_task.outFwdPaired,
            #reverseReads = trimmomatic_pe_task.outRevPaired,
            suffix = "post",
            outputPrefix = outputPrefix
    }

    

    output {
        Array[File] pre_formatHistArray = reformat_pe_task.outHistArray
        #Array[File] post_formatHistArray = if readQc=="yes" then pe_post_reformat.outHistArray else reformat_pe_task.outHistArray
        Array[File] post_formatHistArray = pe_post_reformat.outHistArray
        #File multiqcArray = multiqc_task.multiqcArray
        File? forwardTrimmedPEReads = trimmomatic_pe_task.outFwdPaired
        File? reverseTrimmedPEReads = trimmomatic_pe_task.outRevPaired
        #File forwardTrimmedPEReads = pe_post_reformat.outFqFiles[0]
        #File reverseTrimmedPEReads = pe_post_reformat.outFqFiles[1]
    }
}
