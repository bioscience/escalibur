##############################################################
##############################################################
###
###
### Replicating the Filtering perl module
### author: Noel Faux
### 12/03/2020
###
### one workflows:
###    - single end - filtering_workflow_se
###
### Desc: Indexes the reference genomes - Paired end and SE flows
### Soft: Trimmomatic, BBMAP
###
### Notes: Trimmomatic currently takes output from reformat
###	   not the raw reads
###	   Removed the scatter inside sub workflows and moved
### 	   to the main workflow
###
##############################################################
##############################################################

workflow se_filtering_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "bshaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}
    
    File forwardReads
    File adapter
    String outputPrefix
    String EndType
    Int minLength
    Int Phred
    Int PhredQuality

    	 call filtering_tasks.fastqc_task {
            input:
                forwardReads = forwardReads
        }

        call filtering_tasks.multiqc_task {
            input:
                fastqcArray = fastqcArray,
		outputPrefix = outputPrefix

        }


    call filtering_tasks.one_in_reformat as se_pre_reformat {
        input :
            forwardReads = forwardReads,
            outputPrefix = outputPrefix
    }

    call filtering_tasks.se_trimmomatic_task as seTrimmed {
        input :
            EndType = EndType,
            Phred = Phred,
            PhredQuality = PhredQuality,
	    goutputPrefix = outputPrefux,
            forwardReads = se_pre_reformat.outFqFile,
            minLength  = minLength,
            adapter = adapter
    }

    call filtering_tasks.reformat2 as se_post_reformat {
        input :
        	forwardReads  = seTrimmed.outSEreads,
        	outputPrefix = outputPrefix
    }
    output {
	Array[File] pre_formatHistArray = se_pre_reformat.outHistArray
        File post_formatHistArray = se_post_reformat.outHistArray
        File fowardTrimmedPEReads = se_post_reformat.outFqFile
    }
}
