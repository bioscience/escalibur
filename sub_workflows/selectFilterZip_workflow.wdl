
###############################################################################
# selectFilterZip_workflow
# workflow encapsulationg the gatk steps select -> filter -> bgzip --> tabix
# author: Noel Faux
# 06-March-2020
# Melbourne Data Analyitcs
# Updated and integrated: Bobbie Shaban
# INPUT:
#       - ...
# OUTPUT:
#       - ...
#       - ...
#       - ...
# CHANGES:
#       - ...
# POTENTIAL CHANGES:
#       - ...
#
# ISSUES:
#       - ...
#
###############################################################################

import "./tasks/gatk_SelectVariants_task.wdl" as SelectVariantsTask
import "./tasks/gatk_VariantFiltration_task.wdl" as VariantFiltrationTask
import "./tasks/bgzipTabix_task.wdl" as BgzipTabixTask

workflow selectFilterZip_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "bshaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}
	# Reference genome
	File genoTypeOutput
	Map [String, File] refGenomeDbMap
	String selectType
	String filterName
	String filterExpression
	String singularityContainerString

	call SelectVariantsTask.gatkSelectVariants_task {
		input :
			bestDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			genoTypeOutput = genoTypeOutput,
			selectType = selectType
	}
	call VariantFiltrationTask.gatkVariantFiltration_task {
		input :
			bestDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			selectVariants = gatkSelectVariants_task.selectVariantsOutput,
			filterExpression = filterExpression,
			filterName = filterName,
			selectType = selectType
	}
	call BgzipTabixTask.bgzipTabix_task {
		input :
			variantFilterOut = gatkVariantFiltration_task.VarFilterOutput,
			singularityContainerString = singularityContainerString,
			sampleType = selectType
	}

	output {
		File varSelectOutput = gatkSelectVariants_task.selectVariantsOutput
		File varFilterOutput = gatkVariantFiltration_task.VarFilterOutput
		File bgzipOutput = bgzipTabix_task.bgzipOutput
		File bgzipTabixOutput = bgzipTabix_task.bgzipTabixOutput
	}
}
