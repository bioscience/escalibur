##
# Replicating the Mapping perl module
# author: Noel Faux
# 16/03/2020
#
# The output is an array of arrays of files. The final array size will be one
# for single end reads sampels and two for paired end reads
##

import "./tasks/mapping_task.wdl" as mapping_workflow_tasks

##
# For a given ref database map each sample onto it.
##
workflow sample_mapping {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    Array[Map[String, File]] refDbIndexFiles
    Boolean? pipeToViewBoolean
    String samtoolsParameters
    File forwardReads
    File reverseReads
    String EndType
    String outputPrefix
    String platform
    String? cmdLineParams
    String ReadGroupHeader
    String singularityContainerString

    scatter (bwaRef in refDbIndexFiles) {

	    #Paired end alignment
        if (EndType == "PE") {
		    #perform alignment
            call mapping_workflow_tasks.pe_bwa_mem_task  {
                input :
		            outputPrefix = outputPrefix,
                	bwaRef = bwaRef,
                	forwardReads = forwardReads,
                	reverseReads = reverseReads,
    		        pipeToViewBoolean = pipeToViewBoolean,
                        samtoolsParameters = samtoolsParameters,
                    ReadGroupHeader = ReadGroupHeader,
                    singularityContainerString = singularityContainerString,
                    cmdLineParams = cmdLineParams
            }

	    if (pipeToViewBoolean == false){
    			#convert sam to bam
            	call mapping_workflow_tasks.sam_view_task {
                	input :
    		         	outputPrefix = outputPrefix,
				        singularityContainerString = singularityContainerString,
                    	input_sam = pe_bwa_mem_task.pe_bwa_sam_or_bam_output,
    		          	cmdLineParams = cmdLineParams
            	}
    	   }
        }

	    #single end alignment
        if (EndType == "SE") {
		    #perform alignment
            call mapping_workflow_tasks.se_bwa_mem_task {
                input :
        		bwaRef = bwaRef,
                ReadGroupHeader = ReadGroupHeader,
        		outputPrefix = outputPrefix,
                        samtoolsParameters = samtoolsParameters,
		        singularityContainerString = singularityContainerString,
                forwardReads = forwardReads
            }
    		#view only if no sort is selected i.e. pipeToViewBoolean is false
    		if (pipeToViewBoolean == false){
    			#convert sam to bam
            		call mapping_workflow_tasks.sam_view_task as se_view_task {
                		input :
                            outputPrefix = outputPrefix,
                            singularityContainerString = singularityContainerString,
                    	  	input_sam = se_bwa_mem_task.se_bwa_sam_or_bam_output,
	                    	cmdLineParams = cmdLineParams
            		}
	        }
        }

	    #only run if pipeToBooleanView is false
	    if (pipeToViewBoolean == false){
	    	#sort bam file
        	call mapping_workflow_tasks.bam_sort_task {
            	input :
 	    	        outputPrefix = outputPrefix,
                    singularityContainerString = singularityContainerString,
                	input_bam = sam_view_task.bamViewOutput,
                	outputFormat = "BAM"
        	}
	    }

	   #call samtools stat
	   #Shouldn't use true here: https://gatkforums.broadinstitute.org/wdl/discussion/10487/set-workflow-level-default-value-for-required-task-input
	   call mapping_workflow_tasks.bam_stats_task {
            input :
                input_bam = select_first([bam_sort_task.sortedBamFile, pe_bwa_mem_task.pe_bwa_sam_or_bam_output, se_bwa_mem_task.se_bwa_sam_or_bam_output]),
        	    bwaRef = bwaRef,
                singularityContainerString = singularityContainerString,
        	    outputPrefix = outputPrefix
        }
    } #end scatter

	output {
		Array[File?] sortedBamFilesArray = pe_bwa_mem_task.pe_bwa_sam_or_bam_output
		Array[File?] sortedSEBamFile = se_bwa_mem_task.se_bwa_sam_or_bam_output
		Array[File] sortedBamStats = bam_stats_task.outputBamStats
		Array[Array[File]] outputGrepStats = bam_stats_task.outputGrepStats
	}
}
