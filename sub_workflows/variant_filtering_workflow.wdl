# Need to ensure this is in the .json config and checked against the Escalibur config
# --filter-expression "$cfg{step1}{variant_filtering}{snp}"


import "./tasks/gatk_SelectVariants_task.wdl" as select_vars_task
import "./tasks/gatk_VariantFiltration_task.wdl" as var_filter_task
import "./tasks/vcf_bgzip_tasks.wdl" as vcf_task
import "./tasks/bcftools_task.wdl" as bcf_task
import "./tasks/perl_task.wdl" as perl_task
import "./tasks/build_chr_def_scf_num_lim_task.wdl" as chr_map_def_scf_num_lim_task
import "./tasks/piped_cmd_task.wdl" as p_cmd_task


workflow snp_indel_var_filtering_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        author: "Pasi Korhonen"
        email: "pasi.korhonen@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    Array[String]? chr2Filter = []
    Array[String] modules = []
    File genoTypeOutput
    File OutBaseName = basename(genoTypeOutput, ".vcf.gz")
    File buildChrScript
    File maxDPscript
    File genVarReportScript
    Float ldCutOff
    Int maxIndelSize
    Int nSamples # number of samples used
    Int scfNumLim
    Int scafLenCutOff
    Int scfNumCo
    Int ldWinSize
    Int ldWinStep
    Int PIPE_CMD_threads = 1
    Map[String, File] bestDbMap
    String finalVarOutput = "final_report.txt"
    String indelFilterName
    String snpFilterName
    String indelFilterExpression
    String snpFilterExpression
    String cmdlnArgs = ""
    String piped_programs = ""
    #String vcfChrFilter = if length(chr2Filter) == 0 then "" else "--char ${sep='--char ' chr2Filter}"
    String vcfChrFilter = ""
    String singularityContainerString
    String singularityContainerPath
  

    # select and filter INDEL Var
    call select_vars_task.gatkSelectVariants_task as svindel_tk {
        input :
	    bestDbMap = bestDbMap,
            genoTypeOutput = genoTypeOutput,
            selectType = "INDEL",
	    singularityContainerString = singularityContainerString,
            maxIndelSize = maxIndelSize
    }
    call var_filter_task.gatkVariantFiltration_task as vfindel_tk {
        input :
	    bestDbMap = bestDbMap,
            selectVariants = svindel_tk.selectVariantsOutput,
            filterExpression = indelFilterExpression,
	    singularityContainerString = singularityContainerString,	
            filterName = indelFilterName,
	    selectType = "INDEL"
    }

    # select and filter SNP Var
    call select_vars_task.gatkSelectVariants_task as svsnp_tk {
        input :
	    bestDbMap = bestDbMap,
            singularityContainerString = singularityContainerString,
            genoTypeOutput = genoTypeOutput,
            selectType = "SNP"
    }
    call var_filter_task.gatkVariantFiltration_task as vfsnp_tk {
        input :
	    bestDbMap = bestDbMap,
            selectVariants = svindel_tk.selectVariantsOutput,
            filterName = snpFilterName,
	    singularityContainerString = singularityContainerString,
	    selectType = "SNP",
	    filterExpression = snpFilterExpression
    }

    call vcf_task.vcf_bgzip_task as vcf_rm_filt_all_tk {
        input:
     	    singularityContainerString = singularityContainerString,	
	    singularityContainerPath = singularityContainerPath,
	    VCFinputFile = vfsnp_tk.VarFilterOutput
    }

    call perl_task.perl_task as def_maxDP_task {
        input :
            withFile = true,
            singularityContainerString = singularityContainerString,
	    perlFile = maxDPscript,
            cmdlnArgs = "" + nSamples + " " + vcf_rm_filt_all_tk.VcfBgzipOutputFile + ";"
    }

    call vcf_task.vcf_bgzip_task as vcf_filt_deep_task {
        input :
	    singularityContainerString = singularityContainerString,
	    singularityContainerPath = singularityContainerPath,
            VCFinputFile = vcf_rm_filt_all_tk.VcfOutputFile,
	    maxMeanDP = def_maxDP_task.stdOut
    }

    # vcftools --gzvcf $cfg{step1}{variant_filtering}{vcf} --singletons --stdout >$outpath/singletons.list
    call vcf_task.vcf_bgzip_task as gzvcf_singleton_task {
        input :
	    singularityContainerString = singularityContainerString,
	    singularityContainerPath = singularityContainerPath,
            VCFinputFile = vcf_filt_deep_task.VcfOutputFile
    }

    # vcftools --gzvcf $cfg{step1}{variant_filtering}{vcf} --exclude-positions $outpath/singletons.list --max-missing 1 --max-alleles 2 --minDP 2 --minQ 30 --recode --recode-INFO-all --stdout |bgzip -c >$outpath/high_confidence.vcf.gz
    call vcf_task.vcf_bgzip_task as gzvf_rm_singleton_task {
        input :
            excludePosFile = gzvcf_singleton_task.VcfOutputFile,
	    singularityContainerString = singularityContainerString,
	    singularityContainerPath = singularityContainerPath,
	    VCFinputFile = vfsnp_tk.VarFilterOutput
    }

    ###### prepare the setting for low LD prunning
    call chr_map_def_scf_num_lim_task.build_chr_def_scf_num_lim_task as map_def_scf_lim_task {
        input :
	    bestDbMap = bestDbMap,
	    singularityContainerString = singularityContainerString,
	    buildChrScript = buildChrScript,
            sc_len_co = scafLenCutOff,
            sc_num_co = scfNumCo
    }

    # As plink works using prefixs and in a single dir, we need to chain the calls in a single task
    call p_cmd_task.piped_cmds_task as bcf_annotate_perl_vcf_plink_task {
        input :
	     ldWinSize = ldWinSize,
   	     singularityContainerString = singularityContainerString,
	     singularityContainerPath = singularityContainerPath,
	     ldWinStep = ldWinStep,
	     ldCutOff = ldCutOff,
	     PIPE_CMD_threads = PIPE_CMD_threads,
             outFile = "high_confidence_prunned.vcf.gz",

		piped_programs = "bcftools annotate --threads " + PIPE_CMD_threads + " --rename-chrs " +  map_def_scf_lim_task.mapFileOut  + " " + gzvf_rm_singleton_task.VcfOutputFile + " | " +
			     "perl -ne 'if (/#\\S+ID=(\\d+)/){if($1<=" + map_def_scf_lim_task.scfNumLim + " " + "){print;}}elsif(/^#/){print;}elsif(/^(\\d+)\\s+/){if($1<=" + map_def_scf_lim_task.scfNumLim + "" +"){print;}}' | " +
			     singularityContainerString + " " + "vcftools --vcf - --plink --out plink; " +
                 singularityContainerString + " " + "plink --threads " + PIPE_CMD_threads + " --file plink --make-bed --chr-set " + map_def_scf_lim_task.scfNumLim + "  no-xy no-mt no-y; " +
                 singularityContainerString + " " + "plink2 --threads " + PIPE_CMD_threads + " --bfile plink --indep-pairwise " + ldWinSize + " " + ldWinStep + " " +  ldCutOff + " --chr-set " + map_def_scf_lim_task.scfNumLim + " no-xy no-mt no-y; " +
                 singularityContainerString + " " + "plink2 --threads " + PIPE_CMD_threads + " --bfile plink --make-bed " +  "--out high_confidence_prunned --chr-set " + map_def_scf_lim_task.scfNumLim + "" + " no-xy no-mt no-y; " +
                 singularityContainerString + " " + "plink2 --threads " + PIPE_CMD_threads + " --bfile high_confidence_prunned --chr-set " + map_def_scf_lim_task.scfNumLim + "" + " no-xy no-mt no-y --recode vcf --out high_confidence_pre; " +
                 "cat high_confidence_pre.vcf | perl -ne 'print unless (/CHROM/);if(/CHROM/){s/_\\S+//g;print;}' | " + singularityContainerString + " bgzip -c > high_confidence_prunned.vcf.gz;"
    }

    call perl_task.perl_task as generate_variant_report{
        input :
            withFile = true,
 	    singularityContainerString = singularityContainerString,
            perlFile = genVarReportScript,
            cmdlnArgs = " " + vcf_filt_deep_task.VcfOutputFile + "  " + gzvcf_singleton_task.VcfOutputFile  + " " +
                        " " + gzvf_rm_singleton_task.VcfOutputFile + " " + bcf_annotate_perl_vcf_plink_task.pipedCmdOutput + " " +
                        " " + "final_report.txt" + " "
    }

    output {
        File outInitialFilteredSNPs = vcf_filt_deep_task.VcfOutputFile
        File outHighFedalitySNPs = gzvf_rm_singleton_task.VcfOutputFile
        File outINDELs = vfindel_tk.VarFilterOutput
        File? outFinalVarReport = generate_variant_report.finalVarRep
    }
}
