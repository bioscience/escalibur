#####################################################################################
# QC - mDuplicate work flow
#
# 06/05/2020
#
# QC - "fast" implementation of the pipeline
#    - Consists of calling mark duplicates -> samtools index -> haplotype caller
#
# Runs per sample
#
#
# Input: 	Sorted bam files from sample mapping
# Changes: 	Possibly change output to Map
######################################################################################
####
#
# Workflow representing the intial qc workflow unit before variant calling
# gatk Mark Duplicates -> samtools index -> gatk Haplotype Caller
# author: Noel Faux
# Integration: Bobbie Shaban
####
################### Import tasks ###################################
import "./tasks/gatk_MarkDuplicate_task.wdl" as mark_dups_task
import "./tasks/index_task.wdl" as index_task
import "./tasks/gatk_HaplotypeCaller_task.wdl" as hap_caller_task

workflow qc_mDup_sidx_hc_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    Array[File?] sortedBams
    Int ploidy
    Map[String, File] bestDbMap
    String bestDb
    String base #= basename(sortedBams[0], ".txt")
    String singularityContainerString
    String picardString

    call mark_dups_task.gatk_MarkDuplicates_task as md_task {
        input :
            inputBams = sortedBams,
	    picardString = picardString,
	    singularityContainerString = singularityContainerString,
    	    base = base
    }

    call hap_caller_task.gatk_HaplotypeCaller_task as hap_task {
        input :
            bestDb = bestDb,
            bestDbMap = bestDbMap,
            base = base,
            inputBam = md_task.mDupSortedBam,
	    inputBamIndex = md_task.mDupSortedBamIndex,
            emitRefConf = true,
            ploidy = ploidy,
	    picardString = picardString,
	    singularityContainerString = singularityContainerString
    }

    output {
        File mDupSortedBam = md_task.mDupSortedBam
    	File mDupSortedBamIndex = md_task.mDupSortedBamIndex
    	File? haplo_GVCF = hap_task.GVCF
        Map[String, File?] mDupSortedBamAndGVCFMap = {"bam": md_task.mDupSortedBam, "bamidx": md_task.mDupSortedBamIndex, "gvcf": hap_task.GVCF}
    }
}
