#############################################################
##
## Escalibur - SubWorkflow - Indexing
## Date: 16-03-2020
## Desc: Indexes the reference genomes
## Soft: BWA, picard
##
## TODO: add the descriptions in the parameter_meta section.
#############################################################

#import required tasks
import "./tasks/index_task.wdl" as indexingTask

workflow index_sub_workflow {
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
	author: "Bobbie Shaban"
	email: "bshaban@unimelb.edu.au"
	author: "Pasi Korhonen"
	email: "pasi.korhonen@unimelb.edu.au"
        description: "For each sample/database, index using samtools, bwa and picard."
        }
        parameter_meta {
       	       referenceArray: "itype:Array[Array[File]]: ... "
	       picard: "itype:File: ..."

	       bwaIndexFiles: "otype:Array[Map[String, File]]: ..."
	       indexTest: "otype:Array[Map[String, File]]: ..."
	       indexFasta: "otype:Array[File]: ..."
	       picardDict: "otype:Array[File]: ..."
	       samIndex: "otype:Array[File]: ..."
        }

	Array[Array[File]] referenceArray
	String singularityContainerString

	scatter(ref in referenceArray) {
	    call indexingTask.indexing_bwa_task {
		input :
		    singularityContainerString = singularityContainerString,
		    RefFastaFile = ref[1]
	    }

	    call indexingTask.indexing_sam_task {
		input :
		    singularityContainerString = singularityContainerString,
		    faidx = true,
		    RefFastaFile = ref[1]
	    }
	}

	######################################
	## Capture indexing subworkflow output
	## Array of database index files
	## Should be Array of Arrays?
	## changed bshaban 17-03-2020

	output {
	    Array[Map[String, File]] bwaIndexFiles = indexing_bwa_task.bwaIndexFiles
	    Array[Map[String, File]] indexTest = indexing_bwa_task.indexTest
	    Array[File] indexFasta = indexing_bwa_task.indexFasta
	    Array[File] samIndex = indexing_sam_task.samIndexFile
	}
}
