####
#
# Workflow encapsulating gatk CombineGVCFs -> gatk GenotypeGVCFs
# author: Nole Faux
#
####

import "./tasks/gatk_CombineGVCFs_task" as combineGVCFs_task
import "./tasks/gatk_GenotypeGVCFs_task" as genotypeGVCFs_task

workflow gat_combine_genotype_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    File $referenceDb
	Array[File] refDbIndexFiles
	Array[File] $sample_gvcfs
	File CombineOutput
	File genotypeOutput
        String ploidy
	String singularityContainerString

    call combineGVCFs_task.gatk_CombineGVCFs_task as combineGVCFSs_task {
        input :
            referenceDb = referenceDb,
	    singularityContainerString = singularityContainerString,
            refDbIndexFiles = refDbIndexFiles,
            sample_gvcfs = sample_gvcfs,
            Output = CombineOutput
    }

    call genotypeGVCFs_task.gatk_GenotypeGVCFs_task as genotypeGVCFs_task {
        input :
            referenceDb = referenceDb,
            refDbIndexFiles = refDbIndexFiles,
	    singularityContainerString = singularityContainerString,
            ploidy = ploidy
            combinedSamples = "${combineGVCFSs_task.Output}"
            Output = genotypeOutput
    }

    output {
        genotypeGVCFs_OutFile = "${genotypeGVCFs_task.Output}"
    }
}
