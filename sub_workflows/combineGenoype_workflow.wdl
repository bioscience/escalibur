## workflow encapsulating the gatk combine -> genotype steps
## author: Noel Faux
## 06-March-2020
## Melbourne Data Analyitcs

import "./tasks/gatk_CombineGVCFs_task.wdl" as CombineGVCFsTask
import "./tasks/gatk_GenotypeGVCFs_task.wdl" as GenotypeGVCFsTask

workflow combineGenotype_workflow{
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "bshaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}
	Array[File?] vcfSamples
	Int ploidy
	Map[String, File] bestDbMap
	String singularityContainerString

	# Combine Initial Joint Calling
	call CombineGVCFsTask.gatk_CombineGVCFs_task {
		input:
			bestDbMap = bestDbMap,
			singularityContainerString = singularityContainerString,
			sample_gvcfs = vcfSamples
	}

	call GenotypeGVCFsTask.gatk_GenotypeGVCFs_task {
		input:
			ploidy = ploidy,
			singularityContainerString = singularityContainerString,
			bestDbMap = bestDbMap,
			genoTypeOutput = gatk_CombineGVCFs_task.gvcfOutput
	}

	output {
		File genoTypeOutput = gatk_GenotypeGVCFs_task.gvcfOutput
	}
}
