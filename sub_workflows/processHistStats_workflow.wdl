##
#
# author: Noel Faux
#
# For each sample (alignment) collect the COV, IS, and SN stats then collate these
# with the histogram data from the data filtering step.
#
# Should only be called from the readReports_workflow.wdl
# bobbie: Called from main instead. Much of readReports workflow was redundant
##

import "./tasks/read_report_task.wdl" as reportTask

workflow processStats_workflow {
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
    Array[Array[File]] pre_formatHistArray
    Array[Array[File]] post_formatHistArray
    Array[Array[File]] sortedBamStats
    Array[File] flatSortedBams = flatten(sortedBamStats)
    Array[Map[String, File]] refDbIndexFiles
    Array[Array[Array[File]]] outputGrepStats
    Array[File] grepStats = flatten(flatten(outputGrepStats))
    File ReadSummaryRscript
    String singularityContainerString
    String singularityBindPath
    String singularityContainerPath

    ############### Cycle through databases to process stat files and histograms for each #############
    scatter (bwaRef in refDbIndexFiles){
    	###prep 4 report###
    	call reportTask.prep4Report_task {
            input :
            	flatSortedBams = flatSortedBams,
            	bwaRef = bwaRef,
    	}

    	###create report task###
    	call reportTask.createReport_task {
           	input :
			singularityContainerString = singularityContainerString,
                        singularityBindPath = singularityBindPath,
                        singularityContainerPath = singularityContainerPath,
        		pre_formatHistArray = pre_formatHistArray,
        		post_formatHistArray = post_formatHistArray,
        		isStatFile = prep4Report_task.isStatOut,
        		covStatFile = prep4Report_task.covStatOut,
        		snStatFile = prep4Report_task.snStatOut,
                ReadSummaryRscript = ReadSummaryRscript,
        		outputGrepStats = grepStats
    	}
    }

    output {
    	Array[File] covStats = prep4Report_task.covStatOut
    	Array[File] isStats = prep4Report_task.isStatOut
    	Array[File] snStats = prep4Report_task.snStatOut
    	Array[File] refDBstats = createReport_task.refDBStatsFile
    }
}
