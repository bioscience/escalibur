####################################################################################################
#
# Workflow representing the workflow unit
#  2*(gatk BaseRecalibration --> gatk ApplyBQSR) --> gatk Analyse Covariates -->
#			gatk Haplotype Calling
#
####################################################################################################

import "./tasks/gatk_BaseRecalibration_task.wdl" as BaseRecalibration_Task
import "./tasks/gatk_ApplyBQSR_task.wdl" as ApplyBQSR_Task
import "./tasks/index_task.wdl" as SamtoolsTask
import "./tasks/gatk_AnalyseCovariates_task.wdl" as AnalyseCovariatesTask
import "./tasks/gatk_HaplotypeCaller_task.wdl" as HaplotypeCallerTask

workflow baseRecalibration_workflow {
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "Workflow encapsulating the pipeline, \n\n2*(BaseRecalibration --> ApplyBQSR) --> Analyse Covariates --> Haplotype Calling"
    }
    parameter_meta { #TODO: clean up the meta feilds
        # Inputs:
		bestDb: "itype:String: ..."
		refGenomeDbMap: "itype:Map[String, File]: Containin the reference genome and auxiliary index files"
        sample: "itype:File: Individual genomic sample to call varaients for <file ext or type?>"
		snpSitesMap: "itype:Map[String, File]: ..."
		indelSitesMap: "itype:Map[String, File]: ..."
		ploidy: "itype:Int: number of chormosome sets, 1 = haploid, 2 = diplod, etc"
		emitRefConf: "itype:Boolean: ..."
        # Outputs:
		Initial_RecaibratedSample_Table_File: "otype:File: baseRecal_T1.RecalOutput"
		AfterApply_RecaibratedSample_Table_File: "otype:File: baseRecal_T2.RecalOutput"
		sapmleFistRecal_BQSR_Bam_File: "otype:File: gatkApplyBQSR_T1.recalBQSRoutput"
		sapmleSecondRecal_BQSR_Bam_File: "otype:File: gatkApplyBQSR_T2.recalBQSRoutput"
		RecalPlots_File: "otype:File: gatk_AnalyzeCovariates_task.PlotReport"
		RecalCSV_File: "otype:File: gatk_AnalyzeCovariates_task.CSVReport"
		Varcall_After_RecalSample_GVCF_File: "otype:File: hapCall_task.GVCF"
    }
	Boolean emitRefConf = true
	File sample
	Int ploidy
	Map[String, File] snpSitesMap
	Map[String, File] indelSitesMap
	Map[String, File] refGenomeDbMap
	String bestDb
	String sampleName = basename(sample)
	String picardString
	String singularityContainerString

	call BaseRecalibration_Task.gatkBaseRecalibration_task as baseRecal_T1{
		input :
			refGenomeDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			sample = sample,
			Output = "${sampleName}.initial.recal_data.table",
			snpSitesMap = snpSitesMap,
			indelSitesMap = indelSitesMap
	}

	call ApplyBQSR_Task.gatkApplyBQSR_task as gatkApplyBQSR_T1 {
		input :
			refGenomeDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			sample = sample,
			Output = "${sampleName}.1stBQSR.bam",
			sampleRecalTable = baseRecal_T1.RecalOutput
	}

	call BaseRecalibration_Task.gatkBaseRecalibration_task as baseRecal_T2 {
		input :
			refGenomeDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			sample = gatkApplyBQSR_T1.recalBQSRoutput,
			Output = "${sampleName}.initial.recal_data_after.table",
			snpSitesMap = snpSitesMap,
			indelSitesMap = indelSitesMap
	}

	call ApplyBQSR_Task.gatkApplyBQSR_task as gatkApplyBQSR_T2 {
		input :
			refGenomeDbMap = refGenomeDbMap,
			singularityContainerString = singularityContainerString,
			sample = sample,
			Output = "${sampleName}.2ndBQSR.bam",
			sampleRecalTable = baseRecal_T2.RecalOutput
	}

	call AnalyseCovariatesTask.gatk_AnalyzeCovariates_task {
		input :
			beforeReport = baseRecal_T1.RecalOutput,
			afterReport = baseRecal_T2.RecalOutput,
			singularityContainerString = singularityContainerString,
			plot = "${sampleName}_recal.pdf",
			csv = "${sampleName}_recal.csv"
	}

	call HaplotypeCallerTask.gatk_HaplotypeCaller_task as hapCall_task {
		input :
			bestDb = bestDb,
			bestDbMap = refGenomeDbMap,
			base = "${sampleName}.initial.after_recal",
			inputBam = gatkApplyBQSR_T2.recalBQSRoutput,
			inputBamIndex = gatkApplyBQSR_T2.recalBQSRindex,
    	    		picardString = picardString,
			singularityContainerString = singularityContainerString,
			emitRefConf = emitRefConf,
			ploidy = ploidy
	}

	output {
		File Initial_RecaibratedSample_Table_File = baseRecal_T1.RecalOutput
		File AfterApply_RecaibratedSample_Table_File = baseRecal_T2.RecalOutput
		File sapmleFistRecal_BQSR_Bam_File = gatkApplyBQSR_T1.recalBQSRoutput
		File sapmleSecondRecal_BQSR_Bam_File = gatkApplyBQSR_T2.recalBQSRoutput
		File RecalPlots_File = gatk_AnalyzeCovariates_task.PlotReport
		File RecalCSV_File = gatk_AnalyzeCovariates_task.CSVReport
		File Varcall_After_RecalSample_GVCF_File = hapCall_task.GVCF
	}
}
