use File::Path qw(make_path);
use Data::Dumper;
use strict;
use warnings;

# A list of all the Summary.xls files. File Name <ref_db>.Summary.xls
my @files = @ARGV;

my %rs;
my %rs_ref;

foreach my $f (@files) {
	my @bases = split('\.', $f);
	my $basename = $bases[0];

	open RS, "$f";
	<RS>;

	##Set $temp_ref as basename
	my $temp_ref = $basename;
	while(<RS>){
		my @a = split /\t/;
		my $sample_id = $a[0];
		my $mapped_rate = $a[7];
		my $match_rate = 1 - $a[8];
		my $paired_rate = $a[9];
		my $cover_area = $a[12];

		$rs{mapped_rate}{$sample_id}{$temp_ref} = $mapped_rate;
		$rs{match_rate}{$sample_id}{$temp_ref} = $match_rate;
		$rs{paired_rate}{$sample_id}{$temp_ref} = $paired_rate;
		$rs{cover_area}{$sample_id}{$temp_ref} = $cover_area;

		$rs_ref{$temp_ref} = 0

	}
	close RS;
}

foreach my $meric (keys %rs){
	foreach my $sample_id(keys %{$rs{$meric}}){
		my $temp_top = 0;
		my $temp_top_name = "";
		foreach my $temp_ref(keys %{$rs{$meric}{$sample_id}}){
			if ($rs{$meric}{$sample_id}{$temp_ref} >= $temp_top){
				$temp_top = $rs{$meric}{$sample_id}{$temp_ref};
				$temp_top_name = $temp_ref;
			}
		}
		$rs_ref{$temp_top_name}++;
	}
}

my $temp_top = 0;
my $temp_top_name = "";
foreach my $temp_ref(keys %rs_ref){
	if ($rs_ref{$temp_ref}>$temp_top){
		$temp_top = $rs_ref{$temp_ref};
		$temp_top_name = $temp_ref;
	}
}

print "$temp_top_name\n";
