use Getopt::Long qw(GetOptions);
use strict;
use warnings;

my $related_file;
my $out_file;

GetOptions(
	'rf=s' => \$vcf_file,
	'o=s' => \$out_file
) or die "Usage: $0 -rf RELATEDNESS_FILE -o REORT_FILE\n"


#INDV1	INDV2	N_AaAa	N_AAaa	N1_Aa	N2_Aa	RELATEDNESS_PHI
#1-1	1-1	3.52698e+06	0	3.52698e+06	3.52698e+06	0.5
#1-1	1-2	1.55085e+06	156628	3.52698e+06	3.4146e+06	0.178286

open IN, $related_file;
open OT, $out_file;
while (<IN>){
	my @a = split /\t/;
	if($a[6] =~ /RELATEDNESS_PHI/){print OT $_;next;}
	if($a[0] eq $a[1]){next;}
	if($a[6] >= 0.25){print OT $_;} 
}
close IN;
close OT;

