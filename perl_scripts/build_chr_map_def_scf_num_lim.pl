use strict;
use warnings;

my $i=0;
my $j=0;

my $file = $ARGV[0];
my $sc_len_co = $ARGV[1];
my $sc_num_co = $ARGV[2];
my $ln;

open OUT, ">$ARGV[0]" or die $!;

while($ln=<STDIN>) {
    if ($ln =~ /^(\S+)\s+(\d+)$/){
        $i++;
        print OUT "$1\t$i\n";
        if (($2 >= $sc_len_co) && ($j < $sc_num_co)){
            $j++;
        }
    }
}
close OUT;
print $j."\n";
