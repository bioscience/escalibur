use strict;
use warnings;

my $unfiltered_vcf_file = $ARGV[0];
my $singletons_file = $ARGV[1];
my $high_conf_vcf_file = $ARGV[2];
my $high_conf_prunned_vcf_file = $ARGV[3];
my $outfile = $ARGV[4];

### format a report
my %report;
if ($unfiltered_vcf_file =~ /.gz$/) { open(IN, "gunzip -c $unfiltered_vcf_file |") || die "can’t open pipe to $unfiltered_vcf_file";}
    else { open(IN, $unfiltered_vcf_file) || die "can’t open $unfiltered_vcf_file";}

$report{snv1}{number}=0;
while (<IN>){
    next if (/^#/);
    $report{snv1}{number} ++;
}
close IN;
$report{snv1}{singletons} = `wc -l $singletons_file | cut -d" " -f1`;
$report{snv1}{singletons} = $report{snv1}{singletons} - 1;

if ($high_conf_vcf_file =~ /.gz$/) { open(IN, "gunzip -c $high_conf_vcf_file |") || die "can’t open pipe to $high_conf_vcf_file";}
    else { open(IN, $high_conf_vcf_file) || die "can’t open $high_conf_vcf_file";}
$report{snv2}{number}=0;
while (<IN>){
    next if (/^#/);
    $report{snv2}{number} ++;
}
close IN;

if ($high_conf_prunned_vcf_file =~ /.gz$/) { open(IN, "gunzip -c $high_conf_prunned_vcf_file |") || die "can’t open pipe to $high_conf_prunned_vcf_file";}
    else { open(IN, $high_conf_prunned_vcf_file) || die "can’t open $high_conf_prunned_vcf_file";}
$report{snv3}{number}=0;
while (<IN>){
    next if (/^#/);
    $report{snv3}{number} ++;
}
close IN;
####
open OT, ">$outfile";
print OT "SNV set\tSNV size\tSingleton size\n";
print OT "PASS.SNP.DP.vcf.gz\t$report{snv1}{number}\t$report{snv1}{singletons}\n";
print OT "high_confidence.vcf.gz\t$report{snv2}{number}\t0\n";
print OT "high_confidence_prunned.vcf.gz\t$report{snv3}{number}\t0\n";
close OT;
