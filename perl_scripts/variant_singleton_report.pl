use Getopt::Long qw(GetOptions);
use strict;
use warnings;

my $vcf_file;
my $singleton_file;
my $lowld_high_confidence_vcf_file;
my $out_file;

GetOptions(
	'vcf=s' => \$vcf_file,
	'sf=s' => \$singleton_file,
	'hcf=s' => \$lowld_high_confidence_vcf_file,
	'o=s' => \$out_file
) or die "Usage: $0 -vcf VCF_FILE -sf SINGLETON_FILE -hcf HIGH_CONFIDENCE_VCF_FILE -o REORT_FILE\n"

### format a report
my %report;
if ($vcf_file =~ /.gz$/) { open(IN, "gunzip -c $vcf_file |") || die "can’t open pipe to $vcf_file";}
	else { open(IN, $vcf_file) || die "can’t open $vcf_file";}

$report{snv1}{number}=0;
while (<IN>){
	next if (/^#/);
	$report{snv1}{number} ++;
}
close IN;
$report{snv1}{singletons} = `wc -l $singleton_file | cut -d" " -f1`;
$report{snv1}{singletons} = $report{snv1}{singletons} - 1;

if ($lowld_high_confidence_vcf_file =~ /.gz$/) { open(IN, "gunzip -c $lowld_high_confidence_vcf_file |") || die "can’t open pipe to $lowld_high_confidence_vcf_file";}
	else { open(IN, $lowld_high_confidence_vcf_file) || die "can’t open $lowld_high_confidence_vcf_file";}
$report{snv2}{number}=0;
while (<IN>){
	next if (/^#/);
	$report{snv2}{number} ++;
}
close IN;

if ($lowld_high_confidence_vcf_file =~ /.gz$/) { open(IN, "gunzip -c $lowld_high_confidence_vcf_file |") || die "can’t open pipe to $lowld_high_confidence_vcf_file";}
	else { open(IN, $lowld_high_confidence_vcf_file) || die "can’t open $lowld_high_confidence_vcf_file";}
$report{snv3}{number}=0;
while (<IN>){
	next if (/^#/);
	$report{snv3}{number} ++;
}
close IN;
####
open OT, ">$out_file";
print OT "SNV set\tSNV size\tSingleton size\n";
print OT "PASS.SNP.DP.vcf.gz\t$report{snv1}{number}\t$report{snv1}{singletons}\n";
print OT "high_confidence.vcf.gz\t$report{snv2}{number}\t0\n";
print OT "high_confidence_prunned.vcf.gz\t$report{snv3}{number}\t0\n";
close OT;
