use strict;
use warnings;

my $sample_size = $ARGV[0];
my $vcf_file = $ARGV[1];
open (IN, "zcat $vcf_file|") or die $!;
my @dp;
while (<IN>){
    next if (/#/);
    if (/DP=([\d\.]+)/){push @dp, $1;}
}
my $p50 = int(@dp * .5);
my $avgdp50 = 3*$dp[$p50]/$sample_size;
close IN;
print($avgdp50)
