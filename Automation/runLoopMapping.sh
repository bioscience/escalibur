#!/bin/bash

cd ..
sed 's/"//g;s/,//g;s/://g' Automation/runtimes.json | awk -F"." '{print $NF}' | grep -E '_minutes|_threads|_mem' | awk 'BEGIN{s="sed "}{s=s"s\/: \047\\${"$1"}\047\/: "$2"\/g;"}END{print s" $1"}' | sed 's/sed s/sed "s/1' | rev | sed 's/;/"/1' | rev > sed.sh
for myFile in Automation/tasksOrig/*; do echo $myFile; done | awk -F"/" '{print $NF}' | while read line; do sh sed.sh Automation/tasksOrig/$line > tasks/$line; done
cp Automation/runtimes.json .

for ((i=1;i<=10;i++)); do
java -Dconfig.file=workflow-runtime.slurm.config -jar cromwell-50.jar run workflow-mapping.wdl -i workflow-mapping.json -o workflow-mapping.outputs.json > out.round$i 2> err.round$i
sleep 20
cp runtimes.json runtimes.json.$i
python Automation/adjust.py -i runtimes.json.$i -w "mapping_workflow" > runtimes.json 2> res.round$i
lineCnt=`wc -l res.round$i | awk '{print $1}'`
if [ "$lineCnt" -eq 0 ]; then
   break
fi
sed 's/"//g;s/,//g;s/://g' runtimes.json | awk -F"." '{print $NF}' | grep -E '_minutes|_threads|_mem' | awk 'BEGIN{s="sed "}{s=s"s\/: \047\\${"$1"}\047\/: "$2"\/g;"}END{print s" $1"}' | sed 's/sed s/sed "s/1' | rev | sed 's/;/"/1' | rev > sed.sh
for myFile in Automation/tasksOrig/*; do echo $myFile; done | awk -F"/" '{print $NF}' | while read line; do sh sed.sh Automation/tasksOrig/$line > tasks/$line; done
done
