#!/usr/bin/env python

import sys, os, argparse
from statistics import mean
from stat import S_ISDIR, S_ISREG, ST_CTIME, ST_MODE

#################################################
def options():
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--json', dest='json', help='JSON config', metavar='JSON', default='')
    parser.add_argument('-r', '--runs', dest='runs', help='Run times and memory usage', metavar='RESOURCES', default='')
    parser.add_argument('-w', '--workflow', dest='workflow', help='Cromwell execution workflow directory', metavar='WORKFLOW', default='mapping_workflow')
    arguments = parser.parse_args()
    if arguments.json == '':
        parser.print_help()
        sys.exit(1)
    return arguments

#################################################
def convertToMin(usedTime):
    '''
    '''
    items = usedTime.split(':')[::-1]
    mins = float(items[0]) / 60.0
    mins += float(items[1])
    if len(items) == 3: mins += float(items[2]) * 60
    return int(mins + 1.5)

#################################################
def updateTasks(jsonFile, dTasksRunTime, dTasksMem, multiplier):
    '''
    '''
    taskListRunTime = list(dTasksRunTime.keys())
    taskListMem = list(dTasksMem.keys())
    with open(jsonFile, "r") as handle:
        for line in handle:
            items = line.strip().split(':')
            if "mapping_workflow" in items[0] or "cleaning_workflow" in items[0] or "variants_workflow" in items[0]:
                taskItems = items[0].strip('"').split('.')
                comma = False
                if ',' in items[1]: comma = True
                found = False
                for taskItem in taskItems:
                    if taskItem in taskListRunTime:
                        if "_minutes" in taskItems[-1]:
                            found = True
                            value = int(items[1].strip(','))
                            if comma == True:
                                print ("%s: %s," %(items[0], int(value * multiplier)))
                            else:
                                print ("%s: %s" %(items[0], int(value * multiplier)))
                    if taskItem in taskListMem:
                        if "_mem" in taskItems[-1]:
                            found = True
                            value = int(items[1].strip(','))
                            if comma == True:
                                print ("%s: %s," %(items[0], int(value * multiplier)))
                            else:
                                print ("%s: %s" %(items[0], int(value * multiplier)))
                if found == False:
                    print (line.strip())
            else:
                print (line.strip())

#################################################
def setTasks(jsonFile, dTasksRunTime, dTasksCpu, dTasksMem):
    '''
    '''
    taskListRunTime = list(dTasksRunTime.keys())
    taskListCpu = list(dTasksCpu.keys())
    taskListMem = list(dTasksMem.keys())
    with open(jsonFile, "r") as handle:
        for line in handle:
            items = line.strip().split(':')
            if "mapping_workflow" in items[0] or "cleaning_workflow" in items[0] or "variants_workflow" in items[0]:
                taskItems = items[0].strip('"').split('.')
                comma = False
                if ',' in items[1]: comma = True
                found = False
                for taskItem in taskItems:
                    if taskItem in taskListRunTime:
                        if "_minutes" in taskItems[-1]:
                            found = True
                            if comma == True:
                                print ("%s: %s," %(items[0], dTasksRunTime[taskItem]))
                            else:
                                print ("%s: %s" %(items[0], dTasksRunTime[taskItem]))
                    if taskItem in taskListCpu:
                        if "_threads" in taskItems[-1]:
                            found = True
                            if comma == True:
                                print ("%s: %s," %(items[0], dTasksCpu[taskItem]))
                            else:
                                print ("%s: %s" %(items[0], dTasksCpu[taskItem]))
                    if taskItem in taskListMem:
                        if "_mem" in taskItems[-1]:
                            found = True
                            if comma == True:
                                print ("%s: %s," %(items[0], dTasksMem[taskItem]))
                            else:
                                print ("%s: %s" %(items[0], dTasksMem[taskItem]))
                if found == False:
                    print (line.strip())
            else:
                print (line.strip())

#################################################
def main():
    '''
    '''
    opts = options()
    multiplier = 2.0

    dTasksRunTime, dTasksCpu, dTasksMem = {}, {}, {}
    if opts.runs != '':
        with open(opts.runs, "r") as handle:
            handle.readline()
            for line in handle:
                if "\t\t" in line: continue
                items = line.strip().split('\t')
                try:
                    dTasksRunTime[items[0]].append(convertToMin(items[2]))
                except KeyError:
                    dTasksRunTime[items[0]] = []
                    dTasksRunTime[items[0]].append(convertToMin(items[2]))
    
                try:
                    dTasksCpu[items[0]].append(float(items[4].strip('%')) / 100.0)
                except KeyError:
                    dTasksCpu[items[0]] = []
                    dTasksCpu[items[0]].append(float(items[4].strip('%')) / 100.0)
    
                try:
                    dTasksMem[items[0]].append(float(items[6]) * 1000) # Used in megabytes
                except KeyError:
                    dTasksMem[items[0]] = []
                    dTasksMem[items[0]].append(float(items[6]) * 1000) # Used in megabytes
    
        for key in dTasksRunTime: # At least 60 minutes reserved
            dTasksRunTime[key] = max(int(float(mean(dTasksRunTime[key])) * multiplier), 60)
        for key in dTasksCpu: # At least 1 core required
            dTasksCpu[key] = max(int(float(mean(dTasksCpu[key])) * multiplier), 1)
        for key in dTasksMem: # At least 4 GB required
            dTasksMem[key] = max(int(float(mean(dTasksMem[key])) * multiplier), 4000)
    
        setTasks(opts.json, dTasksRunTime, dTasksCpu, dTasksMem)
    else:
        os.listdir("cromwell-executions/%s" %opts.workflow)
        cromwellPath = "cromwell-executions/%s" %opts.workflow
        entries = [os.path.join(cromwellPath, filename) for filename in os.listdir(cromwellPath)]
        entries = [(os.stat(path), path) for path in entries]
        entries = [(stat[ST_CTIME], path) for stat, path in entries if S_ISDIR(stat[ST_MODE])]
        #print (entries)
        myPath = sorted(entries)[-1][-1]
        for path, dirs, files in os.walk(myPath):
            for filename in files:
                if filename == "rc":
                    with open(os.path.join(path,filename)) as handle:
                        errVal = int(handle.readline().strip())
                        if errVal == 79: # Timeout
                            items = path.split('/')[::-1]
                            for item in items:
                                if 'call-' in item:
                                    myTask = item.replace('call-', '')
                                    dTasksRunTime[myTask] = True
                                    break
                            print (os.path.join(path, filename), errVal, file=sys.stderr)
                        elif errVal == 137: # Out of Memory
                            items = path.split('/')[::-1]
                            for item in items:
                                if 'call-' in item:
                                    myTask = item.replace('call-', '')
                                    dTasksMem[myTask] = True
                                    break
                            print (os.path.join(path, filename), errVal, file=sys.stderr)
                        elif errVal != 0:
                            print ("### Unknown error ...", os.path.join(path, filename), errVal, file=sys.stderr)
                if filename == "stderr":
                    with open(os.path.join(path,filename)) as handle:
                        errVal = 0
                        for line in handle:
                            if "out-of-memory" in line:
                                errVal = 137
                                break
                        if errVal == 137: # Out of Memory
                            items = path.split('/')[::-1]
                            for item in items:
                                if 'call-' in item:
                                    myTask = item.replace('call-', '')
                                    dTasksMem[myTask] = True
                                    break
                            print (os.path.join(path, filename), errVal, file=sys.stderr)
                        elif errVal != 0:
                            print ("### Unknown error ...", os.path.join(path, filename), errVal, file=sys.stderr)
        updateTasks(opts.json, dTasksRunTime, dTasksMem, multiplier)


#################################################
if __name__ == "__main__":
    main()
