# -*- coding: utf-8 -*-
""" 
05/10/2021 - Bobbie Shaban, Noel Faux, Pasi Korhonen

Description:
    This script will take in a number of parameters and will create an
    input file that can be used for the ESCALIBUR WDL workflow

Example:
    $  python ./scripts/inputMaker.py -f fasta -1 "/home/.../escalibur/fastqFiles/*_1*.gz" -2 "/home/.../escalibur/fastqFiles/*_2*.gz" -o input.txt -pl PL -ph 33

usage: inputArgMaker.py [-h] [-o OUTPUTFILE] -1 FIRST -2 SECOND [-ps PHREDS]
                        [-pq PHREDQ] [-ml MINLENGTH] [-pl PLATFORM]
                        [-lb LIBRARY] [-sm SAMPLE] [--debug]
Todo:
    * Throw error if no files in directory or if they are not gzipped or fastq
"""

import sys, os, glob, re, pprint, gzip, argparse
import contextlib
from itertools import islice
from collections import OrderedDict

#####################################################################
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--outputfile', type=str, required=False, default=None, help='Name for the output file')
    parser.add_argument('-1', '--first', type=str, required=True, help='First pair of PE reads or SE reads separated by comma. Wild cards accepted. Use quotation marks: e.g. -1 "a*r1*gz,b*r2*gz" ')
    parser.add_argument('-2', '--second', type=str, required=False, default='', help='Second pair of PE reads separated by comma. Wild cards accepted. Use quotation marks as above.')
    parser.add_argument('-ps', '--phredS', required=False, type=str, default='33', help='Phred score of FASTQ reads i.e. 33 or 64')
    parser.add_argument('-pq', '--phredQ', required=False, type=str, default='20', help='Phred cut-off used to filter FASTQ reads by quality')
    parser.add_argument('-ml', '--minlength', required=False, type=str, default='60', help='Minimum length of reads after trimming')
    parser.add_argument('-pl', '--platform', required=False, type=str, default='ILLUMINA', help='Name of the sequencer platform')
    parser.add_argument('-lb', '--library', required=False, type=str, default='LIB1', help='Library ID to BAM header (optional). User must check and edit sample ID in resulting output file.')
    parser.add_argument('-sm', '--sample', required=False, type=str, default='SM1', help='Sample ID to BAM header (optional). User must check and edit sample ID in resulting output file.')
    parser.add_argument('--debug', action="store_true", help='Do you want to turn on Debug Mode?')
    args = parser.parse_args()

    pairedend = True
    if args.second == '': pairedend = False

    # if debug is set to true run
    if args.debug:
        print("\nDebugging info START\nNumber of arguments: %d" %len(sys.argv))
        print('Argument List:', str(sys.argv))
        print('Opts string:', str(args), "\n")

    # return file list and size from read files function
    fileListRaw1, fileListRaw2 = [], []
    for files in args.first.split(','):
        fileListRaw1 += readFiles_glob(files)
    for files in args.second.split(','):
        fileListRaw2 += readFiles_glob(files)
    fileListRaw1.sort(key=lambda s: s[1], reverse=True)
    fileListRaw2.sort(key=lambda s: s[1], reverse=True)
    if args.second != "" and len(fileListRaw1) != len(fileListRaw2):
        print ("### FATAL: numbers of PE files do not match - forward: %d vs. reverse: %d ..." %(len(fileListRaw1), len(fileListRaw2)))
        sys.exit(-1)
    #print (fileListRaw1)
    #print (fileListRaw2)

    with openWrite(args.outputfile) as handle:
        if pairedend == True:
            for i in range(len(fileListRaw1)):
                files = fileListRaw1[i]
                sample1, size, identity = str(files[1]), str(files[0]), str(files[2])
                sample2 = fileListRaw2[i][1]
                baseName = os.path.basename(sample1).split('.')[0]
                handle.write("{}\tPE\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".
                                 format(baseName, args.minlength, args.phredS,
                                        args.platform, args.phredQ, args.library,
                                        identity, args.sample, identity, sample1, sample2))
        else:
            for files in fileListRaw1:
                sample, size, identity = str(files[1]), str(files[0]), str(files[2])
                baseName = os.path.basename(sample).split('.')[0]
                handle.write("{}\tSE\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".
                                 format(baseName,args.minlength,args.phredS,args.platform,
                                        args.phredQ, args.library, identity, args.sample,
                                        identity, sample))

    if args.debug:
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(fileListRaw1)
        pp.pprint(fileListRaw2)

######################### Functions #################################
def readFiles_glob(files):
    """ Returns list of files
        Args:
            param1 (string): Path to the fasta files
        Returns:
            list: returns list of files
    """
    try:
        vals = []
        for name in glob.glob(files):
            #print (directory + fs)
            #for name in sorted(glob.glob(os.path.join(directory + fs ))):
            file_size = os.path.getsize(name)
            #get id from first line in forward read
            if 'gz' in name:
                with gzip.open(name, 'rb') as f:
                    for l in islice(f, 0, None):
                        try:
                            first_line = re.search("@(.+?)C0", "l {}".format(l)).group(1)
                        except AttributeError:
                            try:
                                first_line = re.search("@(.+?):", "l {}".format(l)).group(1)
                            except AttributeError:
                                 print("not found\n")
                        break
            else:
                with open(name, 'r') as inf:
                    fline = inf.readline()
                    first_line = re.search("@(.+?)C0", "fline {}".format(fline)).group(1)
            vals.append((file_size, name, first_line))

        #sort files by size then reverse - probably better way to do this
        vals.sort(key=lambda s: s[1], reverse=True)
    except:
        print(files + ' does not exist\n')
    return vals

#####################################################################
@contextlib.contextmanager
def openWrite(filename = None):
    if filename and filename != '-':
        handle = open(filename, 'w')
    else:
        handle = sys.stdout
    try:
        yield handle
    finally:
        if handle is not sys.stdout: handle.close()
                                                    
########################### Main call ###############################
if __name__ == "__main__":
     main()
