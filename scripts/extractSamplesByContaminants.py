#!/usr/bin/env python

import os, sys, argparse

#################################################
def options():
    #${singularityContainerString} python ${script} -i ${contaminantFile} -r ${sep=',' contaminantRefIndices};
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--conf', dest='confFile', help='Configuration file describing contaminant genomes per sample', metavar='CONF', default='')
    parser.add_argument('-r', '--refs', dest='refFiles', help='Genomes for contaminants', metavar='GENOMES', default='')
    arguments = parser.parse_args()
    if arguments.confFile == '':
        parser.print_help()
        sys.exit(1)
    return arguments

#################################################
def main():
    '''
    '''
    opts = options()
    refFiles = opts.refFiles.strip('"').strip("'").strip('{').strip('}').split("},{")
    referenceFiles = []
    dIndices = {}
    for refFile in refFiles:
        indexFiles = refFile.split(", ")
        for indexFile in indexFiles:
            key, item = indexFile.split(": ")
            key, item = key.strip('"'), item.strip('"')
            #if "Dingo" in key and "Dingo" in refFile:
            #    print ("## Dingo", key, item, refFile)
            if key == "index":
                basename = os.path.basename(item)
                #if os.path.basename(refFile) == key:
                referenceFiles.append((basename, item))
                dIndices[basename] = indexFiles
                break
    #for key, refFile in referenceFiles:
    #    print (refFile)
    #sys.exit(-1)

    dSamples = {}
    with open(opts.confFile, "r") as handle:
        for line in handle:
            items = line.strip().split('\t')
            if len(items) < 3: continue
            sampleId, bamFile = items[0], items[1]
            refFileBasenames = []
            for i in range(2, len(items)):
                for key, refFile in referenceFiles:
                    if key in items[i]:
                        refFileBasenames.append(key)
                        break
            dSamples[sampleId] = bamFile, refFileBasenames

    #keys, items = [], []
    #print ("{", file=sys.stderr)
    with open("debug.output", 'w') as handle:
        for sampleId in dSamples:
            bamFile, refFileBasenames = dSamples[sampleId][0], dSamples[sampleId][1]
            print ("%s\t%s\n" %(sampleId, bamFile))
            handle.write("%s\t%s\n" %(sampleId, bamFile))
        #print ("%s\n" %sampleId)
        #print ("%s\n" %bamFile, file=sys.stderr)
        #print ("  {", file=sys.stderr)
        #for refFileBasename in refFileBasenames:
        #    indexFiles = dIndices[refFileBasename] 
        #    for indexFile in indexFiles:
        #        key, item = indexFile.split(": ")
        #        key, item = key.strip('"'), item.strip('"')
        #        keys.append(key)
        #        items.append(item)
        #        print (    "%s:%s, " %(key, item), file=sys.stderr)
        #print ("  },", file=sys.stderr)
    #print ("}", file=sys.stderr)
    #print ("%s\n" %'\t'.join(keys), file=sys.stderr)
    #print ("%s\n" %'\t'.join(items), file=sys.stderr)
    #sys.exit(-1)


#################################################
if __name__ == "__main__":
    main()
