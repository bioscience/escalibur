#!/usr/bin/env python

import sys, argparse

#################################################
def options():
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--conf', dest='confFile', help='Configuration file describing contaminant genomes per sample', metavar='CONF', default='')
    arguments = parser.parse_args()
    if arguments.confFile == '':
        parser.print_help()
        sys.exit(1)
    return arguments


#################################################
def main():
    '''
    '''
    opts = options()
    refs = set()
    with open(opts.confFile) as handle:
        for line in handle:
            items = line.strip().split('\t')
            if len(items) > 2:
                for i in range(2, len(items)):
                    refs.add(items[i])
    print ('\n'.join(sorted(refs)))

#################################################
if __name__ == "__main__":
    main()
