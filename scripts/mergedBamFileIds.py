#!/usr/bin/env python

import sys, argparse

#################################################
def options():
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--readsFile', dest='readsFile', help='Generated file describing FASTQ reads', metavar='READS', default='')
    parser.add_argument('-r', '--refFileLabel', dest='refFileLabel', help='All generated BAM files', metavar='BAMS', default='')
    parser.add_argument('-p', '--path', dest='path', help='Root path', metavar='ROOTPATH', default='')
    arguments = parser.parse_args()
    if arguments.readsFile == '' or arguments.refFileLabel == '':
        parser.print_help()
        sys.exit(1)
    return arguments


#################################################
def main():
    '''
    '''
    opts = options()
    refGenomeId = opts.refFileLabel
    sampleIds = set()
    with open(opts.readsFile, "r") as handle:
        for line in handle:
            items = line.strip().split('\t')
            sampleIds.add(items[8])
    for sampleId in sorted(sampleIds):
            sampleId = "%s.%s" %(sampleId, refGenomeId)
            print ("%s/output/%s.MarkDup.bam" %(opts.path, sampleId))

#################################################
if __name__ == "__main__":
    main()
