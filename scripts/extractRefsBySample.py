#!/usr/bin/env python

import os, sys, argparse

#################################################
def options():
    #${singularityContainerString} python ${script} -i ${contaminantFile} -r ${sep=',' contaminantRefIndices};
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--conf', dest='confFile', help='Configuration file describing contaminant genomes per sample', metavar='CONF', default='')
    parser.add_argument('-s', '--sampleId', dest='sampleId', help='Sample identifier', metavar='SAMPLEID', default='')
    parser.add_argument('-r', '--refs', dest='refFiles', help='Genomes for contaminants', metavar='GENOMES', default='')
    arguments = parser.parse_args()
    if arguments.confFile == '':
        parser.print_help()
        sys.exit(1)
    return arguments

#################################################
def main():
    '''
    '''
    opts = options()
    refFiles = opts.refFiles.strip('"').strip("'").strip('{').strip('}').split("},{")
    referenceFiles = []
    #print (opts.sampleId)
    dIndices = {}
    for refFile in refFiles:
        indexFiles = refFile.split(", ")
        for indexFile in indexFiles:
            key, item = indexFile.split(": ")
            key, item = key.strip('"'), item.strip('"')
            #if "Dingo" in key and "Dingo" in refFile:
            #    print ("## Dingo", key, item, refFile)
            if key == "index":
                basename = os.path.basename(item)
                #if os.path.basename(refFile) == key:
                basename = basename[:-3]
                referenceFiles.append((basename, item))
                dIndices[basename] = indexFiles
                break
    #for key, refFile in referenceFiles:
    #    print (key, refFile)
    #sys.exit(-1)

    refFileBasenames = []
    with open(opts.confFile) as handle:
        for line in handle:
            items = line.strip().split('\t')
            if len(items) < 3: continue
            sampleId, bamFile = items[0], items[1]
            #print (sampleId, opts.sampleId)
            if sampleId == opts.sampleId:
                for i in range(2, len(items)):
                    for key, refFile in referenceFiles:
                        #print (key, items[i])
                        if key in items[i]:
                            refFileBasenames.append(key)
                            break
                break

    #print (dIndices.keys())
    items = []
    for refFileBasename in refFileBasenames:
        #print (refFileBasename)
        indexFiles = dIndices[refFileBasename]
        #print (indexFiles) 
        for indexFile in indexFiles:
            key, item = indexFile.split(": ")
            key, item = key.strip('"'), item.strip('"')
            items.append("%s\t%s" %(key, item))
            #print ("%s\t%s\n" %(key, item)) #, file=sys.stderr)
        break
    print ('\n'.join(items))
    with open("debug.output", 'w') as handle:
        handle.write("%s\n" %'\n'.join(items))
    #print ("%s\n" %'\t'.join(keys), file=sys.stderr)
    #print ("%s\n" %'\t'.join(items), file=sys.stderr)
    #sys.exit(-1)


#################################################
if __name__ == "__main__":
    main()
