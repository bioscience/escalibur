#!/usr/bin/env python

import sys, argparse

#################################################
def options():
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-i', '--readsFile', dest='readsFile', help='Generated file describing FASTQ reads', metavar='READS', default='')
    parser.add_argument('-b', '--bamFiles', dest='bamFiles', help='All generated BAM files', metavar='BAMS', default='')
    arguments = parser.parse_args()
    if arguments.readsFile == '' or arguments.bamFiles == '':
        parser.print_help()
        sys.exit(1)
    return arguments


#################################################
def main():
    '''
    '''
    opts = options()
    #bamFiles = opts.bamFiles.split(",")
    bamFiles = []
    with open(opts.bamFiles) as handle:
        for line in handle:
            bamFiles = line.strip().split(',')
    dBamFiles = {}
    for bamFile in bamFiles:
        key = bamFile.split('/')[-1].split(".")[0]
        try:
            dBamFiles[key].append(bamFile)
        except KeyError:
            dBamFiles[key] = []
            dBamFiles[key].append(bamFile)
    dSamples = {}
    with open(opts.readsFile, "r") as handle:
        for line in handle:
            items = line.strip().split('\t')
            sampleId, readFile = items[8], items[10]
            bamFile = dBamFiles[items[0]]
            refGenomeId = bamFile[0].split('.')[1]
            sampleId = "%s.%s" %(sampleId, refGenomeId)
            if len(bamFile) > 1:
                print ("# FATAL: only one BAM file expected. Found %d ..." %len(bamFile))
            #bamFile = "%s.%s.%s.sorted.bestDb.bam.MarkDup.bam" %(items[0], ~{refLabel}, items[1])
            #if ~{mode} == "independent":
            #   bamFile += ".2ndBQSR.bam"
            try:
                dSamples[sampleId].append(bamFile[0])
            except KeyError:
                dSamples[sampleId] = []
                dSamples[sampleId].append(bamFile[0])
    for sampleId in dSamples:
        print ('\t'.join(dSamples[sampleId]))
        print (sampleId, file=sys.stderr)
        #with open("%s.bamfiles" %sampleId, "w") as handle:
        #	 for bamFile in dSamples[sampleId]:
        #        handle.write("%s\n" %bamFile)


#################################################
if __name__ == "__main__":
    main()
