#!/usr/bin/env python

import os, sys, argparse
import re

#################################################
def args():
    parser = argparse.ArgumentParser('usage: python %prog -1 pathogen.sam -2 host.sam')
    parser.add_argument('-1', '--sam1', dest='sam1', help='SAM file mapped to pathogens (sorted by readname)', metavar='SAM1', default='-')
    parser.add_argument('-2', '--sam2', dest='sam2', help='SAM file mapped to host (sorted by readname)', metavar='SAM2', default='')
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    arguments = parser.parse_args()
    if arguments.sam2 == '':
        parser.print_help()
        sys.exit(1)
    return arguments

#################################################
def compareHits(hits1, hits2): # 1 = pathogen, 2 = host
    '''
    '''
    strs1, strs2 = [], []
    lengths1, indels1 = [], []
    for items in hits1:
        flag = int(items[1])
        if flag & 0xf04 == 0: # mapped and not a duplicate, and passes vendor filters
            cigar = items[5]
            quality = int(items[4])
            if quality < 10: continue
            length, indel = 0, 0
            #for size, action in re.findall('(\d+)([MIDNS])', cigar): # Match, Insert, Deletion, introN, Softclip
            for size, action in re.findall('(\d+)([MID])', cigar): # match
                size = int(size)
                if action in ['M']: # match
                    length += size
                if action in ['I', 'D']: # match
                    indel += size
            lengths1.append(length)
            indels1.append(indel)
            strs1.append('\t'.join(items))
        
    if len(strs1) > 0:
        for items in hits2:
            flag = int(items[1])
            if flag & 0x904 == 0 and flag & 0x3 == 3: # mapped, not secondary or supplement, and properly paired
                cigar = items[5]
                quality = int(items[4])
                if quality < 10: continue
                length, indel = 0, 0
                for size, action in re.findall('(\d+)([MID])', cigar): # match
                    size = int(size)
                    if action in ['M']: # match
                        length += size
                    if action in ['I', 'D']: # match
                        indel += size
                if length > min(lengths1):
                    #if indel <= max(indels1):
                    #if indel == 0:
                    strs2.append('\t'.join(items))
    if len(strs2) == 0:
        strs1 = []
    return strs1, strs2

    
#################################################
def main():
    '''
    '''
    opts = args()
    eof = False
    prevId1, prevId2 = None, None
    fileDesc = opts.sam1
    hits1, hits2 = [], []
    prevItems1 = None
    if opts.sam1 == '-': fileDesc = 0 # sys.input
    with open(fileDesc) as handleSam1:
        with open(opts.sam2) as handleSam2:
            for line in handleSam2:
                if line[0] == '@':
                    print (line.strip(), file=sys.stdout)
                    continue
                items2 = line.strip().split('\t')
                id2 = items2[0]
                #if id2 == "DP8400008865TLL1C001R0031034105":
                #    print ("##### Found item in %s ..." %opts.sam2)
                if prevId2 != None and prevId2 != id2:
                    hits1, found, items1 = [], False, None
                    while True:
                        if prevItems1 == None:
                            line = handleSam1.readline()
                            if not line:
                                eof = True
                                break
                            if line[0] == '@':
                                #pass
                                print (line.strip(), file=sys.stderr)
                                continue
                            else:
                                items1 = line.strip().split('\t')
                        else:
                            items1 = prevItems1
                            prevItems1 = None
                        id1 = items1[0]
                            #if id1 == "DP8400008865TLL1C001R0031034105":
                            #    print ("##### Found item in %s ..." %opts.sam1)
                        if id1 == prevId2:
                                found = True
                                hits1.append(items1)
                        elif found == True:
                                strs1, strs2 = compareHits(hits1, hits2)
                                if len(strs1) > 0 and len(strs2) > 0:
                                    for str1 in strs1:
                                        print (str1, file=sys.stderr)
                                    for str2 in strs2:
                                        print (str2, file=sys.stdout)
                                found = False
                                prevItems1 = items1
                                #hits1.append(items1)
                                #prevId1 = id1
                                break
                    hits2 = []
                if eof == True: break
                hits2.append(items2)
                prevId2 = id2


#################################################
if __name__ == "__main__":
    main()
