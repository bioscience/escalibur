###############################################################################
# Haplotype Caller
# INPUT:
#       - Sorted Bam file from mark duplicates
# OUTPUT:
#       - File GVCF: output vcf with haplotypes
# CHANGES:
#       - None as of yet
# POTENTIAL CHANGES:
# 	- latest version of gatk i.e. 4.0.17 allows for bams to be loaded from http
#	https://github.com/broadinstitute/gatk/releases
###############################################################################


task gatk_HaplotypeCaller_task {
	Boolean emitRefConf = false
	File inputBam # The sample
	File? inputBamIndex ## bam index from gatk_markDuplicates
	Int ploidy
	Int HAP_CALL_mem
	Int HAP_CALL_threads
	Int HAP_CALL_minutes
	Map[String, File] bestDbMap
	String base
	String singularityContainerString
	String picardString
	String bestDb

	command {
	    echo ${bestDbMap['refFasta']}
		echo ${bestDbMap['refFastaIndex']}
		echo ${bestDbMap['refDict']}

	    if [[ ${inputBam} =~ ${basename(bestDb)} ]]; then
		      ${singularityContainerString} gatk HaplotypeCaller \
                      ${true='--emit-ref-confidence GVCF' false='' emitRefConf} \
                      --reference ${bestDbMap['refFasta']} \
                      -ploidy ${ploidy} \
                      --input ${inputBam} \
                      -native-pair-hmm-threads ${HAP_CALL_threads} \
		      --output ${base}_rawLikelihoods.g.vcf
	    fi
	 }
	 runtime {
        	runtime_minutes: '${HAP_CALL_minutes}'
        	cpus: '${HAP_CALL_threads}'
        	mem: '${HAP_CALL_mem}'
  	 }
    	output {
        	File GVCF = "${base}_rawLikelihoods.g.vcf"
  	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }

}
