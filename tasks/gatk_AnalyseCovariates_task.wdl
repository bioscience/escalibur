
task gatk_AnalyzeCovariates_task {
	File beforeReport
	File afterReport
	Int ANA_COV_mem
	Int ANA_COV_threads
	Int ANA_COV_minutes
	String plot
	String csv
	String singularityContainerString

	command {

		${singularityContainerString} gatk AnalyzeCovariates \
			--before-report-file ${beforeReport} \
			--after-report-file ${afterReport} \
			--plots-report-file ${plot} \
			--intermediate-csv-file ${csv};
	}
	runtime{
	 runtime_minutes: '${ANA_COV_minutes}'
         cpus: '${ANA_COV_threads}'
         mem: '${ANA_COV_mem}'
	}
	output {
		File PlotReport = "${plot}"
		File CSVReport = "${csv}"
	}
	 meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }

}
