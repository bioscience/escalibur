task sortFilesBySample {
	String singularityContainerString
	File readsFile
        Array[File?] bamFiles
        File script

	command {
		#${singularityContainerString} python ${script} -i ${readsFile} -b ${sep=',' bamFiles};
                echo "${sep=',' bamFiles}" > bamFiles.txt
                ${singularityContainerString} python ${script} -i ${readsFile} -b bamFiles.txt
		#echo "python ${script} -i ${readsFile} -b ${sep=',' bamFiles}" | ${singularityContainerString} bash -c "read -u 0 line; set -- \$line; exec \"\$@\""
	}
	#runtime {
        #       runtime_minutes: '${BMT_minutes}'
        #       cpus: '${BMT_threads}'
        #       mem: '${BMT_mem}'
        #}
	output {
                # Sample name is copied to the filename before postfix
                #Array[File] sampleSortedFiles = glob("*.bamfiles")
                #Array[Array[File]] inputSamples = read_tsv(inputSamplesFile)
		Array[String] sampleIds = read_lines(stderr())
                Array[Array[File]] sampleSortedFiles = read_tsv(stdout())
	}
	
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}

task createMergedBamFileIds {
	String singularityContainerString
        String singularityBindPath
	File readsFile
        String refLabel
        File script

	command {
		${singularityContainerString} python ${script} -i ${readsFile} -r ${refLabel} -p ${singularityBindPath};
	}
	#runtime {
        #       runtime_minutes: '${BMT_minutes}'
        #       cpus: '${BMT_threads}'
        #       mem: '${BMT_mem}'
        #}
	output {
		Array[File] mergedBamFiles = read_lines(stdout())
	}
	
	meta {
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}

task extractContaminantRefs {
        String singularityContainerString
        File contaminantFile
        File script

	command {
		${singularityContainerString} python ${script} -i ${contaminantFile}
	}
	#runtime {
        #       runtime_minutes: '${BMT_minutes}'
        #       cpus: '${BMT_threads}'
        #       mem: '${BMT_mem}'
        #}
	output {
                Array[File] references = read_lines(stdout())
	}
	
	meta {
                author: "Liina Kinkar"
                email: "liina.kinkar@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}

task samplesByContaminants {
	String singularityContainerString
	File contaminantFile
        Array[Map[String, File]] contaminantRefIndices
        File script

	command {
		${singularityContainerString} python ${script} -i ${contaminantFile} -r "${sep=',' contaminantRefIndices}";
	}
	#runtime {
        #       runtime_minutes: '${BMT_minutes}'
        #       cpus: '${BMT_threads}'
        #       mem: '${BMT_mem}'
        #}
	output {
                # Sample name is copied to the filename before postfix
                Array[Array[String]] sampleIdsAndBamFiles = read_tsv(stdout())
                #Array[File] sampleBamFiles = read_lines(stderr())
                #Array[Map[String, File]] sampleContaminantRefs = contaminantRefIndices[sampleBamFiles]
                #Object sampleContaminantRefs = read_json(stderr())
                #Array[Map[String, File?]] sampleContaminantRefs = read_tsv(stdout())
		#Array[String] sampleIds = read_lines(stderr())
                #Array[Array[File]] sampleSortedFiles = read_tsv(stdout())
	}
	meta {
                author: "Liina Kinkar"
                email: "liina.kinkar@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}

task refsBySample {
	String singularityContainerString
	File contaminantFile
        Array[Map[String, File]] contaminantRefIndices
        String sampleId
        File script
        Int RBS_minutes
        Int RBS_threads
        Int RBS_mem

	command {
		#cat ${write_map(contaminantRefIndices[0])} > write_map.test;
		${singularityContainerString} python ${script} -i ${contaminantFile} -s ${sampleId} -r "${sep=',' contaminantRefIndices}"; \
	}
	runtime {
               runtime_minutes: '${RBS_minutes}'
               cpus: '${RBS_threads}'
               mem: '${RBS_mem}'
        }
	output {
                Map[String, File] sampleContaminantRefs = read_map(stdout())
                #Array[Array[File]] sampleIdsAndBamFiles = read_tsv(stdout())
                #Array[File] sampleBamFiles = read_lines(stderr())
                #Array[Map[String, File]] sampleContaminantRefs = contaminantRefIndices[sampleBamFiles]
                #Object sampleContaminantRefs = read_json(stderr())
		#Array[String] sampleIds = read_lines(stderr())
                #Array[Array[File]] sampleSortedFiles = read_tsv(stdout())
	}
	meta {
                author: "Liina Kinkar"
                email: "liina.kinkar@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
