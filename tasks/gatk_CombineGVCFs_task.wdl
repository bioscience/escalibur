#################
# Takes in an array of raw VCF files for combineing
# INPUT:
#       - ref*: reference files for best database
#       - gvcfArray: Array with raw vcf files and null values removed
# OUTPUT:
#       - gvcfOutput: combined gvcf files
# CHANGES:
#       - create interval file for paralell processing
# NOTES:
#
#################


task gatk_CombineGVCFs_task {
	Array[File?] sample_gvcfs
	Array[File] gvcfArray = select_all(sample_gvcfs)
	Int COMB_VCF_threads
	Int COMB_VCF_mem
	Int COMB_VCF_minutes
	Map[String, File] bestDbMap
	String singularityContainerString

	command {

		echo "Running gatk gatk_CombineGVCFs ..";
		${singularityContainerString} gatk CombineGVCFs \
			--reference ${bestDbMap['refFasta']} \
			--variant ${sep=" --variant " gvcfArray} \
			--output combined.g.vcf;
		echo ". Done\n";
	}
	runtime {
           runtime_minutes: '${COMB_VCF_minutes}'
           cpus: '${COMB_VCF_threads}'
           mem: '${COMB_VCF_mem}'
        }
	output {
		File gvcfOutput = "combined.g.vcf"
	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }

}
