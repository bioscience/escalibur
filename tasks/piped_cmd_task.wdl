# flexiable task to allow program chaining to stdout or file

task piped_cmds_task {
    Int PIPE_CMD_minutes
    Int PIPE_CMD_threads
    Int PIPE_CMD_mem
    Int? ldWinSize
    Int? ldWinStep
    Float? ldCutOff
    String? piped_programs
    String outFile = "high_confidence_prunned.vcf.gz" 
    String singularityContainerString
    String singularityContainerPath

    command {
        ${singularityContainerString} ${piped_programs}
    }
    runtime {
           runtime_minutes: '${PIPE_CMD_minutes}'
           cpus: '${PIPE_CMD_threads}'
           mem: '${PIPE_CMD_mem}'
    }
    output {
        File pipedCmdOutput = if defined(outFile) then "${outFile}" else stdout()
    }
	 meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
