###############################################################################
# Mark duplicates
# INPUT:
#       - Sorted Bam files from mapping subworkflow
# OUTPUT:
#       - File sortedBamFile: bam file after duplicates are marked
#       - File sortedBamFileIndex: Index for the sorted bam file
#       - File bamMetrics: stats (metrics) for the bam file
# CHANGES:
#       - None as of yet
# POTENTIAL CHANGES:
# 	- look into validation stringency, tmp_dir, records_in_ram
#
# ISSUES:
# 	- needs VALIDATION_STRINGENCY=SILENT as the bam headers isn't formed properly
###############################################################################


task gatk_MarkDuplicates_task {
	Array[File?] inputBams
	Int MARK_DUPS_minutes
	Int MARK_DUPS_threads
	Int MARK_DUPS_mem
	String base
	String picardString
	String singularityContainerString

	command {
                #echo '${sep=' ' inputBams}'
		${singularityContainerString} java -jar ${picardString}  MarkDuplicates \
			-I ${sep=' -I ' inputBams} \
		  	-O ${base}.MarkDup.bam \
			--CREATE_INDEX true \
			--VALIDATION_STRINGENCY SILENT \
		  	-M ${base}.MarkDup.txt;
	}
	runtime {
                runtime_minutes: '${MARK_DUPS_minutes}'
                cpus: '${MARK_DUPS_threads}'
                mem: '${MARK_DUPS_mem}'
        }
	output {
        	File mDupSortedBam = "${base}.MarkDup.bam"
        	File mDupSortedBamIndex = "${base}.MarkDup.bai"
		File mDupMetricsFile = "${base}.MarkDup.txt"
        }
   meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
