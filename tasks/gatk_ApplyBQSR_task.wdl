task gatkApplyBQSR_task{
	Int APP_BQSR_mem
	Int APP_BQSR_threads
	Int APP_BQSR_minutes
	File sample
	File sampleRecalTable
	Map[String, File] refGenomeDbMap
	String Output
	String singularityContainerString

	command {

		echo "Running ApplyBQSR .."
		${singularityContainerString} gatk ApplyBQSR \
			-R ${refGenomeDbMap['refFasta']} \
			-I ${sample} \
			-O ${Output} \
			--bqsr-recal-file ${sampleRecalTable};

		${singularityContainerString} samtools index ${Output};
		echo ". Done\n";
	}
	runtime {
        runtime_minutes: '${APP_BQSR_minutes}'
        cpus: '${APP_BQSR_threads}'
        mem: '${APP_BQSR_mem}'
    }
	output {
		# Need to check if the outpur should also be a Map, which includes the index files
		File recalBQSRoutput = "${Output}"
		File recalBQSRindex = "${Output}.bai"
	}
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
