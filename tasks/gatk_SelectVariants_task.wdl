#################
# Selects variants from raw combined GVCF file
# INPUT:
#       - ref*: reference files for best database
#       - gvcfArray: Array with raw vcf files and null values removed
# OUTPUT:
#       - gvcfOutput: combined gvcf files
# CHANGES:
#       - create interval file for paralell processing
# NOTES:
#
#################


task gatkSelectVariants_task {
	File genoTypeOutput
	Int? maxIndelSize
	Int SEL_VAR_minutes
	Int SEL_VAR_mem
	Int SEL_VAR_threads
	Map[String, File] bestDbMap
	String? selectType
	String maxIndelSizeFlag = if defined(maxIndelSize) then "--max-indel-size ${maxIndelSize}" else ""
	String singularityContainerString 

	command {
		echo "Running SelectVariants ..";
		${singularityContainerString} gatk SelectVariants \
			  	--reference ${bestDbMap['refFasta']} \
			  	--variant ${genoTypeOutput} \
			  	--select-type-to-include ${selectType} \
				--output selectVariants.${selectType}.vcf \
				${maxIndelSizeFlag}; 
		echo ". Done\n";
	}
	runtime {
           runtime_minutes: '${SEL_VAR_minutes}'
           cpus: '${SEL_VAR_threads}'
           mem: '${SEL_VAR_mem}'
        }
	output {
		File selectVariantsOutput = "selectVariants.${selectType}.vcf"
	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
