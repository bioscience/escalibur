#################
# Trims paired end reads according to user set parameters
# INPUT:
#       - ref*: reference files for best database
#       - selectVariants: output from variants task
#	- filter: filter name
#	- filterExpression: expressions to filters
#	- selectType: indel or snp
# OUTPUT:
#       - filtered vcf files according to snp or indel run
# CHANGES:
#       - Change static reference files to use the MAP refDbIndexFiles
#       - allow for multiple filters or keep as one string
# NOTES:
#
#################


task gatkVariantFiltration_task {
	File selectVariants
	Int VAR_FILT_mem
	Int VAR_FILT_threads
	Int VAR_FILT_minutes
	Map[String, File] bestDbMap
	String filterName
	String filterExpression
	String selectType
	String singularityContainerString

	command {
		echo  "Running Variant Filtration ..";
		${singularityContainerString} gatk VariantFiltration \
			  	--reference ${bestDbMap['refFasta']}  \
			  	--variant ${selectVariants} \
				--filter-name ${filterName} \
			  	--filter-expression "${filterExpression}" \
				--output filtered_variants.${selectType}.vcf
		echo ". Done\n";
	}
	runtime {
	   runtime_minutes: '${VAR_FILT_minutes}'
           cpus: '${VAR_FILT_threads}'
           mem: '${VAR_FILT_mem}'
	}
	output {
		File VarFilterOutput = "filtered_variants.${selectType}.vcf"
		File VarFilterOutputIndex = "filtered_variants.${selectType}.vcf.idx"
	}
	 meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
