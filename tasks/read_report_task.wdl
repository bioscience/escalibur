###############################################################################
## Read Summary File Task
## INPUT:
##       - histograms and bwa index files
## OUTPUT:
##       - String naming best db
## CHANGES:
##       - None as of yet
## POTENTIAL CHANGES:
## Author: Noel Faux
## Updated and Integrated: Bobbie Shaban
################################################################################

task prep4Report_task {
	Array[File] flatSortedBams
	Int PREP_REPORT_threads
	Int PREP_REPORT_mem
	Int PREP_REPORT_minutes
	Map[String, File] bwaRef
	String refIndex = basename(bwaRef['index'], ".fa")
	String outputDir = "redundant"

	command<<<
		for file in ${sep=" " flatSortedBams}; do
			if [[ $file = *.${refIndex}.* ]]; then
				cat $file | grep ^COV | cut -f 2- >> ${refIndex}.cat.COV.stat.txt
                cat $file | grep ^IS | cut -f 2- >> ${refIndex}.cat.IS.stat.txt;
                cat $file | grep ^SN | cut -f 2- | sed 's/ //g;' >> ${refIndex}.cat.SN.stat.txt;
			fi
		done
	>>>
	runtime {
		runtime_minutes: '${PREP_REPORT_minutes}'
                cpus: '${PREP_REPORT_threads}'
                mem: '${PREP_REPORT_mem}'
	}
	output {
		File covStatOut = "${refIndex}.cat.COV.stat.txt"
		File isStatOut  = "${refIndex}.cat.IS.stat.txt"
		File snStatOut  = "${refIndex}.cat.SN.stat.txt"
	}
 	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}

task createReport_task {
	Array[Array[File]] pre_formatHistArray
	Array[File] flatPreArray = flatten(pre_formatHistArray)
	Array[Array[File]] post_formatHistArray
	Array[File] flatPostArray = flatten(post_formatHistArray)
	Array[File] outputGrepStats
	File isStatFile
	File covStatFile
	File snStatFile
	File ReadSummaryRscript
	Int CREATE_REPORT_mem
	Int CREATE_REPORT_threads
	Int CREATE_REPORT_minutes
	String outputDir = "redundant"
	String refIndex = basename(isStatFile, ".IS.stat.txt")
	String singularityContainerString
	String singularityBindPath
        String singularityContainerPath

	command {
		#${singularityContainerString} Rscript --vanilla ${ReadSummaryRscript} ${outputDir} ${refIndex}.xls ${sep=' ' outputGrepStats} ${sep=' ' flatPreArray}  ${sep=' ' flatPostArray};
		echo "Rscript --vanilla ${ReadSummaryRscript} ${outputDir} ${refIndex}.xls ${sep=' ' outputGrepStats} ${sep=' ' flatPreArray} ${sep=' ' flatPostArray}" | singularity run -B ${singularityBindPath} ${singularityContainerPath} bash -c "read -u 0 line; set -- \$line; exec \"\$@\""
	}
	runtime {
        runtime_minutes: '${CREATE_REPORT_minutes}'
        cpus: '${CREATE_REPORT_threads}'
        mem: '${CREATE_REPORT_mem}'
	}
	output {
		File refDBStatsFile = "${refIndex}.xls"
	}
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}

task findBestRefDBFromReport_task {
	Array[File] refDBstats
	File findDbScript
	Int FIND_DB_minutes
	Int FIND_DB_mem
	Int FIND_DB_threads
	String singularityContainerString

	command {
		perl ${findDbScript} ${sep=' ' refDBstats} > best.ref
        }

	output {
		#String bestDB = read_string(stdout())
		String bestDB = read_string("best.ref")
		File bestRef = "best.ref"
	}
	runtime {
    	runtime_minutes: '${FIND_DB_minutes}'
    	cpus: '${FIND_DB_threads}'
    	mem: '${FIND_DB_mem}'
	}
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
