##
# Paired end alignment
##
task pe_bwa_mem_task {
	Int PE_BWA_MEM_threads
	Int PE_BWA_MEM_minutes
	Int PE_BWA_MEM_mem
	Boolean pipeToViewBoolean
        String samtoolsParameters
	File forwardReads
	File reverseReads
	Map[String, File] bwaRef
	String ReadGroupHeader
	String outputPrefix
    String? outputFormat
	String? cmdLineParams
	String refBase = basename(bwaRef['index'], ".fa")
    String singularityContainerString
	String? pipeViewCmdLine = if defined(pipeToViewBoolean) then "| ${singularityContainerString} samtools view -@ ${PE_BWA_MEM_threads} -bS -h ${samtoolsParameters} ${cmdLineParams} - | ${singularityContainerString} samtools sort -@ ${PE_BWA_MEM_threads} - > \"${outputPrefix}.${refBase}.PE.sorted.bam\"" else " > \"${outputPrefix}.${refBase}.PE.sam;\""

	command {

		echo "Doing a paired end bwa alignment ..";

		${singularityContainerString} bwa mem ${bwaRef['index']} -t ${PE_BWA_MEM_threads} \
			-R ${ReadGroupHeader} \
			${forwardReads} ${reverseReads} ${pipeViewCmdLine}
	}

	output {
		File pe_bwa_sam_or_bam_output =  select_first(["${outputPrefix}.${refBase}.PE.sorted.bam", "${outputPrefix}.${refBase}.PE.sam"])
	}
	runtime {
        runtime_minutes: '${PE_BWA_MEM_minutes}'
        cpus: '${PE_BWA_MEM_threads}'
        mem: '${PE_BWA_MEM_mem}'
    }
	 meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }

}

##
# Single end alignment
##
task se_bwa_mem_task {
	Int SE_BWA_MEM_threads
    Int SE_BWA_MEM_minutes
    Int SE_BWA_MEM_mem
	Boolean? pipeToViewBoolean
        String samtoolsParameters
    Map[String, File] bwaRef
    File forwardReads
    String ReadGroupHeader
	String outputFormat
	String outputPrefix
	String? cmdLineParams
	String refBase = basename(bwaRef['index'], ".fa")
	String singularityContainerString
	String? pipeViewCmdLine = if defined(pipeToViewBoolean) then "| ${singularityContainerString} samtools view -bS ${samtoolsParameters} --threads ${SE_BWA_MEM_threads} ${cmdLineParams} | ${singularityContainerString} samtools sort --output-fmt ${outputFormat} -o ${outputPrefix}.${refBase}.PE.sorted" else "> ${outputPrefix}.${refBase}.SE.sam;"

	command {
		echo "Doing a paired end bwa alignment ..";

		${singularityContainerString} bwa mem ${bwaRef['index']} -t ${SE_BWA_MEM_threads} \
                        -R ${ReadGroupHeader} \
                        ${forwardReads} ${pipeViewCmdLine}
	}
	output {
		File se_bwa_sam_or_bam_output = "${outputPrefix}.${refBase}.SE.sam"
	}
	runtime {
                runtime_minutes: '${SE_BWA_MEM_minutes}'
                cpus: '${SE_BWA_MEM_threads}'
                mem: '${SE_BWA_MEM_mem}'
        }
}

#convert sam to bam
task sam_view_task {
	Int SAM_VIEW_threads
    Int SAM_VIEW_minutes
    Int SAM_VIEW_mem
	File input_sam
	String outputPrefix
	String? cmdLineParams
	String? viewParams
	String singularityContainerString

	command {
		echo "Coverting bwa align to sam .bam file ..";
		${singularityContainerString} samtools view ${input_sam}\
			-bS \
			-f ${viewParams} \
			-threads ${SAM_VIEW_threads} \
			${cmdLineParams} \
			-o ${outputPrefix}.bam
	}
	output {
		File bamViewOutput = "${outputPrefix}.bam"
	}
	runtime {
                runtime_minutes: '${SAM_VIEW_minutes}'
                cpus: '${SAM_VIEW_threads}'
                mem: '${SAM_VIEW_mem}'
        }
}

#sort bam file
task bam_sort_task {
	Int SAM_SORT_threads
    Int SAM_SORT_minutes
    Int SAM_SORT_mem
    File input_bam
    String outputPrefix
	String? outputFormat
	String singularityContainerString

	command {
		${singularityContainerString} samtools sort ${input_bam} \
			--threads ${SAM_SORT_threads} \
			-o ${outputPrefix}.sorted.bam \
			--output-fmt ${outputFormat}
	}
	output {
		File? sortedBamFile = "${outputFormat}"
	}
	runtime {
        runtime_minutes: '${SAM_SORT_minutes}'
        cpus: '${SAM_SORT_threads}'
        mem: '${SAM_SORT_mem}'
    }
}

#stats for bam file
task bam_stats_task {
	Int BAM_STATS_threads
    Int BAM_STATS_minutes
    Int BAM_STATS_mem
    File input_bam
	Map[String, File] bwaRef
	String suffix = basename(bwaRef['index'], ".fa")
    String outputPrefix
	String singularityContainerString

	command {
		echo "Collecting the alignment stats ..";
		${singularityContainerString} samtools stats --threads ${BAM_STATS_threads} ${input_bam} > ${outputPrefix}.${suffix}.stats.txt;
		cat ${outputPrefix}.${suffix}.stats.txt | grep ^COV | cut -f 2- > ${outputPrefix}.${suffix}.COV.stat.txt
        cat ${outputPrefix}.${suffix}.stats.txt | grep ^IS | cut -f 2- > ${outputPrefix}.${suffix}.IS.stat.txt;
        cat ${outputPrefix}.${suffix}.stats.txt | grep ^SN | cut -f 2- | sed 's/ //g;' > ${outputPrefix}.${suffix}.SN.stat.txt;
	}
	output {
		File outputBamStats = "${outputPrefix}.${suffix}.stats.txt"
		Array[File] outputGrepStats = glob("*.stat.*")
	}
	runtime {
                runtime_minutes: '${BAM_STATS_minutes}'
                cpus: '${BAM_STATS_threads}'
                mem: '${BAM_STATS_mem}'
        }
}

task bam_merge_task {
	Int BAM_MERGE_threads
    Int BAM_MERGE_minutes
    Int BAM_MERGE_mem
	String outputPrefix
	Array[File] inputBamFiles
	Boolean nr = true
	String singularityContainerString

	command {
		echo "megering alignments..";
		${singularityContainerString} samtools merge ${true='-nr' false='' nr} \
					   --threads '${BAM_MERGE_threads}' \
					   "${outputPrefix}.merged.bam" \
					   "${sep=' ' inputBamFiles}";
		echo ".Done\n";
	}
	output {
		File outFile = "${outputPrefix}.merged.bam"
	}
	runtime {
		runtime_minutes: '${BAM_MERGE_minutes}'
		cpus: '${BAM_MERGE_threads}'
		mem: '${BAM_MERGE_mem}'
	}
}
