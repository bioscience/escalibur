###############################################################################
# Create best Db
# INPUT:
#       - Sorted Bam files from mapping subworkflow
# OUTPUT:
#       - MAP of best db reference, index and dictionary
# CHANGES:
#       - None as of yet
# POTENTIAL CHANGES:
#
# ISSUES:
###############################################################################


task create_best_db_map_task {
	File referenceFastaFile
	Int CRE_BDB_minutes
	Int CRE_BDB_threads
	Int CRE_BDB_mem
	String bestDb
	String base = basename(referenceFastaFile, ".fa")
	String singularityContainerString
	String picardString

	command {

		if [[ ${referenceFastaFile} =~ ${ basename(bestDb) } ]]; then
			${singularityContainerString} samtools faidx ${referenceFastaFile};
			${singularityContainerString} java -jar ${picardString} CreateSequenceDictionary REFERENCE=${referenceFastaFile} OUTPUT=${referenceFastaFile}.dict;
			cp ${referenceFastaFile}.dict ${base}.dict
			cp ${referenceFastaFile}.fai ${base}.fa.fai
			cp ${referenceFastaFile} ${base}.fa
		fi
	}
	runtime {
		runtime_minutes: '${CRE_BDB_minutes}'
		cpus: '${CRE_BDB_threads}'
		mem: '${CRE_BDB_mem}'
    	}
	output {
		File? referenceFastaIndex = "${base}.fa.fai"
		File? referenceSeqDict = "${base}.dict"
		File? referenceFastaFileOutput = "${base}.fa"
    	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }

}
