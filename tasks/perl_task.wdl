task perl_task {
	Boolean withFile
	File? perlFile
	Int PERL_TASK_minutes
	Int PERL_TASK_threads
	Int PERL_TASK_mem
	String? perlCommand
	String? perlCmdlnFlags
	String? cmdlnArgs
	String singularityContainerString

	command {
		${singularityContainerString} perl ${perlFile} ${cmdlnArgs}
	}
	runtime {
           runtime_minutes: '${PERL_TASK_minutes}'
           cpus: '${PERL_TASK_threads}'
           mem: '${PERL_TASK_mem}'
        }
	output {
		String stdOut = read_string(stdout())
		String stdErr = read_string(stderr())
		File? finalVarRep = "final_report.txt"
	}
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
    	description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
