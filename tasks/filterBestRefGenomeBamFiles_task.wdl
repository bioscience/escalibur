
task filterBestRefGenomeBamFiles_task {
    Array[File?] unfilteredBamFiles
    Int FILT_BAM_threads
    Int FILT_BAM_minutes
    Int FILT_BAM_mem
    String bestDb

    command {
        for f in ${sep=' ' unfilteredBamFiles}
        do
            if [[ $f =~ ${ basename(bestDb)} ]]; then
                bn=$(basename -s .bam $f)
                ln -s $f $bn.bestDb.bam
            fi
        done
     }
     runtime {
	runtime_minutes: '${FILT_BAM_minutes}'
        cpus: '${FILT_BAM_threads}'
        mem: '${FILT_BAM_mem}'
     } 
     output {
        Array[File] filteredBamFiles = glob("*.bestDb.bam")
     }
 meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }

}

