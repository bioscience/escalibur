task vcf_bgzip_task {
	Boolean recode
	Boolean recodeInfoAll
	Boolean removeFilteredAll
	Boolean withStdOut
	Boolean bgzip
	Boolean gzvcf
	Boolean meanDp
	Boolean missing
	Boolean singletons
	Boolean excludePos
	Boolean minQ
	Boolean minDP
	Boolean convertPlink
	Boolean relatedness2
	Boolean withOut
	Boolean alleles
	File? VCFinputFile
	File? excludePosFile
	File? mapLimFile
	Float? maxMissing
	Float? minMeanDP
	Float? maxMeanDP
	Int? mQVal
	Int? mDP
	Int? maxAlleles
	Int VCF_BZIP_minutes
	Int VCF_BZIP_threads
	Int VCF_BZIP_mem
	String outputFile = "intermediaryVCF.vcf"
	String vcfOutput = "rm_singleton_file.vcf"
	String chrcmdArrayString = ""
	String singularityContainerString
        String singularityContainerPath

	command {
	
		echo "Running vcftools ..";
		${singularityContainerString} vcftools ${if gzvcf then "--gzvcf " + VCFinputFile + " " else "--vcf " +  VCFinputFile + " "} ${chrcmdArrayString} ${if meanDp then "--min-meanDP " + minMeanDP + " --max-meanDP " +  maxMeanDP + "" else ""} 	${if missing then "--max-missing " + maxMissing + " " else " "} ${if alleles then "--max-alleles " + maxAlleles + " " else " "} ${true="--singletons" false="" singletons} ${if excludePos then "--exclude-positions " + excludePosFile + "" else ""}  ${if minQ then "--minQ " + mQVal + " " else " "}  ${if minDP then "--minDP " + mDP + " " else " "} ${true="--recode" false="" recode} ${true="--recode-INFO-all" false="" recodeInfoAll} ${true="--remove-filtered-all" false="" removeFilteredAll} ${true="--plink" false="" convertPlink} ${true="--relatedness2" false="" relatedness2} ${ if withOut then "--out " + outputFile + "" else ""} ${true="--stdout " false="" withStdOut} 	${if withStdOut then " > " + vcfOutput + ";" else ";"}
			${if bgzip then "cat " + vcfOutput + " | bgzip -c > " + vcfOutput + ".gz;" else ""}
		echo ". Done\n";
	}
	runtime {
           runtime_minutes: '${VCF_BZIP_minutes}'
           cpus: '${VCF_BZIP_threads}'
           mem: '${VCF_BZIP_mem}'
        }
	output {
		File? VcfBgzipOutputFile = "rm_singleton_file.vcf.gz"
		File VcfOutputFile = "rm_singleton_file.vcf"
	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }


}
