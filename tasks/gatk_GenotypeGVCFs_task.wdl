#################
# Takes in combined VCF and runs genotypeGVCFs
# INPUT:
#       - ref*: reference files for best database
#       - combinedSamples: CombinedGVCF file
# OUTPUT:
#       - gvcfOutput: GVCF output file
# CHANGES:
# NOTES:
#
#################


task gatk_GenotypeGVCFs_task {
	Int GENO_TYPE_mem
	Int GENO_TYPE_threads
	Int GENO_TYPE_minutes
	File genoTypeOutput
	Map[String, File] bestDbMap
	String ploidy
	String singularityContainerString

	command {
		echo "Running gatk GenotypeGVCFs ..";
		${singularityContainerString} gatk GenotypeGVCFs \
			--reference ${bestDbMap['refFasta']} \
			-ploidy ${ploidy} \
			--variant ${genoTypeOutput} \
			--output full_genotype_output.vcf;
		echo ". Done\n";
	}
	runtime {
           runtime_minutes: '${GENO_TYPE_minutes}'
           cpus: '${GENO_TYPE_threads}'
           mem: '${GENO_TYPE_mem}'
        }
	output {
		File gvcfOutput = "full_genotype_output.vcf"
	}
	meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
