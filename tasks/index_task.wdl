
# Initial ref database index test work flow for Escalibur

#####Annotate ME
#bshaban 17-3-2020

task indexing_bwa_for_cleaning_task {

    File RefFastaFile
    Int IBT_minutes
    Int IBT_threads
    Int IBT_mem
    String RefBase = basename(RefFastaFile)
    String singularityContainerString

    command {

        #using -a bwtsw for long genomes
        #Should be an option set in the json file
        echo "bwa index ${RefFastaFile} ... running .";
	${singularityContainerString} bwa index ${RefFastaFile} -p ${RefBase}
	    #test with wdl sub
		cat ${RefFastaFile} > ${RefBase};
        echo ".. Done";
    }
    runtime {
        runtime_minutes: '${IBT_minutes}'
        cpus: '${IBT_threads}'
        mem: '${IBT_mem}'
    }
    output {
		# this works but there is no way to tell which order the files are in
		# While it's likely that it's in alphabetical order it's best to
		# ensure order
        	File indexFasta = "${RefBase}"
		#Map[String, File] bwaIndexFiles = {"index":"${RefBase}","amb":"${RefBase}.amb", "ann":"${RefBase}.ann", "bwt":"${RefBase}.bwt", "pac":"${RefBase}.pac", "sa":"${RefBase}.sa"}
		Map[String, File] bwaIndexFiles = {"${RefBase}":"${RefBase}","amb":"${RefBase}.amb", "ann":"${RefBase}.ann", "bwt":"${RefBase}.bwt", "pac":"${RefBase}.pac", "sa":"${RefBase}.sa"}
    }
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        author: "Liina Kinkar"
        email: "liina.kinkar@unimelb.edu.au"
        author: "Pasi Korhonen"
        email: "pasi.korhonen@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}

task indexing_bwa_task {

    File RefFastaFile
    Int IBT_minutes
    Int IBT_threads
    Int IBT_mem
    String RefBase = basename(RefFastaFile)
    String singularityContainerString

    command {

        #using -a bwtsw for long genomes
        #Should be an option set in the json file
        echo "bwa index ${RefFastaFile} ... running .";
	${singularityContainerString} bwa index ${RefFastaFile} -p ${RefBase}.fa
	    #test with wdl sub
		cat ${RefFastaFile} > ${RefBase}.fa;
        echo ".. Done";
    }
    runtime {
        runtime_minutes: '${IBT_minutes}'
        cpus: '${IBT_threads}'
        mem: '${IBT_mem}'
    }
    output {
		# this works but there is no way to tell which order the files are in
		# While it's likely that it's in alphabetical order it's best to
		# ensure order
        	File indexFasta = "${RefBase}.fa"
		Map[String, File] bwaIndexFiles = {"index":"${RefBase}.fa","amb":"${RefBase}.fa.amb", "ann":"${RefBase}.fa.ann", "bwt":"${RefBase}.fa.bwt", "pac":"${RefBase}.fa.pac", "sa":"${RefBase}.fa.sa"}
		Map[String, File] indexTest = {"${RefBase}":"${RefBase}.fa","amb":"${RefBase}.fa.amb", "ann":"${RefBase}.fa.ann", "bwt":"${RefBase}.fa.bwt", "pac":"${RefBase}.fa.pac", "sa":"${RefBase}.fa.sa"}
    }
    meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}

task indexing_sam_task {
	Boolean faidx
	Boolean index
	File RefFastaFile
	Int IST_minutes
	Int IST_threads
	Int IST_mem
	String singularityContainerString
	String refBase = basename(RefFastaFile)

	command {

		echo "samtools index/faidx ${RefFastaFile} ... running .";
		${singularityContainerString} samtools ${true='faidx' false='' faidx} ${true='index' false='' index} \
			${RefFastaFile};
		cp ${RefFastaFile}*.fai ${refBase}.fai 
		echo ".. Done\n";
	}
	runtime {
		runtime_minutes: '${IST_minutes}'
		cpus: '${IST_threads}'
		mem: '${IST_mem}'
	}
	output {
		#File samIndexFile = sub("${RefFastaFile}", "fasta", "fasta.fai")
		File samIndexFile = "${refBase}.fai"
	}
}

task indexing_picard_task {
	String singularityContainerString
	File RefFastaFile
	String picardString
	String RefBase = basename(RefFastaFile)
	#Int IPT_minutes
	#Int IPT_threads
	#Int IPT_mem

	command {

		echo "picard CreateSequenceDictionary r=${RefBase} O=${RefBase} ... running .";
		${singularityContainerString} java -jar ${picardString} CreateSequenceDictionary r=${RefFastaFile} O=${RefBase}.dict;
		echo ".. Done\n";
	}
	#runtime {
        #        runtime_minutes: '${IPT_minutes}'
        #        cpus: '${IPT_threads}'
        #        mem: '${IPT_mem}'
        #}
	output {
		File piccardDicFile = "${RefBase}.dict"
	}
}

task indexing_bam_file {
	File bamFile
	Int IST_minutes
	Int IST_threads
	Int IST_mem
	String singularityContainerString
	String bamBase = basename(bamFile)

	command {
		echo "samtools index ${bamFile} ... running .";
		${singularityContainerString} samtools index ${bamFile};
		cp ${bamFile}*.bai ${bamBase}.bai 
		ln -s ${bamFile} ${bamBase}
		echo ".. Done\n";
	}
	runtime {
		runtime_minutes: '${IST_minutes}'
		cpus: '${IST_threads}'
		mem: '${IST_mem}'
	}
	output {
		#File samIndexFile = sub("${RefFastaFile}", "fasta", "fasta.fai")
		File bamLinkFile = "${bamBase}"
		File bamIndexFile = "${bamBase}.bai"
	}
}

task indexing_ref_task {
	#Boolean faidx
	#Boolean index
	File RefFastaFile
	String picardString
	Int IST_minutes
	Int IST_threads
	Int IST_mem
	String singularityContainerString
	String refBase = basename(RefFastaFile)

	command {
		echo "samtools index/faidx ${RefFastaFile} ... running .";
		${singularityContainerString} samtools faidx ${RefFastaFile};
		cp ${RefFastaFile}*.fai ${refBase}.fa.fai 
		echo "picard CreateSequenceDictionary r=${refBase} O=${refBase} ... running .";
		${singularityContainerString} java -jar ${picardString} CreateSequenceDictionary r=${RefFastaFile} O=${refBase}.dict;
	        cat ${RefFastaFile} > ${refBase}.fa;
		echo ".. Done\n";
	}
	runtime {
		runtime_minutes: '${IST_minutes}'
		cpus: '${IST_threads}'
		mem: '${IST_mem}'
	}
	output {
		Map[String, File] indexFiles = {"refFasta":"${refBase}.fa","refFastaIndex":"${refBase}.fa.fai", "refDict":"${refBase}.dict"}
	}
}
