#################
## zips vcf and create tabix file
## INPUT:
##       - variantFilterOutput: output from variant filtration
##	 - sampleType: sample type i.e. indel/snp
## OUTPUT:
##       - bgzipOut: Bgzipped file
##	 - bgZipTbi: Tabix file
## CHANGES:
##	 - check to see BCFTools and tabix are latest versions
## NOTES:
##	- requires bgzip and tabix
##	- bgzip is part of BCFTools
##################
#
task bgzipTabix_task {
	File variantFilterOut
	Int BZIP_TBI_minutes
	Int BZIP_TBI_mem
	Int BZIP_TBI_threads
	String sampleType
	String singularityContainerString
	String vcfName = basename(variantFilterOut)

	command {

		mv ${variantFilterOut} ./${vcfName};
		${singularityContainerString} bgzip ${vcfName} -c > ${vcfName}.t.gz;
		${singularityContainerString} tabix ${vcfName}.t.gz;
		mv ./${vcfName}.t.gz.tbi ./${vcfName}.gz.tbi
		mv ./${vcfName}.t.gz ./${vcfName}.gz
	}
	runtime {
	   	runtime_minutes: '${BZIP_TBI_minutes}'
        cpus: '${BZIP_TBI_threads}'
        mem: '${BZIP_TBI_mem}'
	}
	output {
		File bgzipOutput = "${vcfName}.gz"
		File bgzipTabixOutput = "${vcfName}.gz.tbi"
	}

	 meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
