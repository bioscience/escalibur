############################################
#
# Copyout task
# author:  Bobbie Shaban & Noel Faux
# Should be reusable inbetween tasks
# Write all file in Array to the desired ouptut directory
#
##########################################


task writeOutput_task {
    Int copyOutputRunThreads
    Int copyOutputRunMinutes
    Int copyOutputRunMem
    String outputDir = "redundant"
    Array[File] filesArray

    command {
	    cp ${sep=' ' filesArray} '${outputDir}';
    }
    runtime {
        runtime_minutes: '${copyOutputRunMinutes}'
        cpus: '${copyOutputRunThreads}'
        mem: '${copyOutputRunMem}'
    }
    output {
		# No output needed here as data is now written to a specific path location
    }
     meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }

}
