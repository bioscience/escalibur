task bcftools_task {
	Int threads
	File chrMapList
	File vcfInputFile
	String singularityContainerString

	command {
		${singularityContainerString} bcftools annotate --threads ${threads} \
			--rename-chrs ${chrMapList} ${vcfInputFile};
	}
	output{
		File Output = stdout()
	}

         meta {
                author: "Noel Faux"
                email: "nfaux@unimelb.edu.au"
                author: "Bobbie Shaban"
                email: "bshaban@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
