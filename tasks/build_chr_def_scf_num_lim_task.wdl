task build_chr_def_scf_num_lim_task {
    File buildChrScript
    Int BCD_SNL_minutes
    Int BCD_SNL_threads
    Int BCD_SNL_mem
    Int sc_len_co # scaffold length cutoff
    Int sc_num_co # scaffold number cutoff
    Map[String, File] bestDbMap
    String singularityContainerString

    command {

        ${singularityContainerString} cat ${bestDbMap['refFastaIndex']} | \
	sort -nrk2 | cut -f 1,2 | perl ${buildChrScript} \
        scaffold_map.list ${sc_len_co} ${sc_num_co}
    }
    runtime {
        runtime_minutes: '${BCD_SNL_minutes}'
        cpus: '${BCD_SNL_threads}'
        mem: '${BCD_SNL_mem}'
    }
    output {
        File mapFileOut = "scaffold_map.list"
        String scfNumLim = read_string(stdout())
    }
     meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
