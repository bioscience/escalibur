task gatkBaseRecalibration_task {
	Int BASE_RCL_minutes
	Int BASE_RCL_mem
	Int BASE_RCL_threads
	File sample
	Map[String, File] refGenomeDbMap
	Map[String, File] snpSitesMap
	Map[String, File] indelSitesMap
	String Output
	String singularityContainerString

	command {

		echo "Running the base recalibration ..";
		${singularityContainerString} gatk BaseRecalibrator \
			-R ${refGenomeDbMap['refFasta']} \
			-I ${sample} \
			-O ${Output} \
			--known-sites ${snpSitesMap['bgz']} \
			--known-sites ${indelSitesMap['bgz']};
		echo ". Done\n";
	}
	runtime {
		runtime_minutes: '${BASE_RCL_minutes}'
        	cpus: '${BASE_RCL_threads}'
        	mem: '${BASE_RCL_mem}'
	}
	output {
		File RecalOutput = "${Output}"
	}
	meta {
        author: "Noel Faux"
        email: "nfaux@unimelb.edu.au"
        author: "Bobbie Shaban"
        email: "bshaban@unimelb.edu.au"
        description: "<DESCRIPTION>"
    }
    parameter_meta {
        # Inputs:
        Input1: "itype:<TYPE>: <DESCRIPTION>"
        # Outputs:
        Output1: "otype:<TYPE>: <DESCRIPTION>"
    }
}
