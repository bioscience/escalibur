##
# Clean reads against given reference genomes
##
task cleanBams_task {
        File sampleBamFile
        Map[String, File] contaminantRef
	#String refBase = basename(contaminantRef['index'], ".fa")
	String refBase = basename(contaminantRef['index'])
	Int CLEAN_BAMS_minutes
	Int CLEAN_BAMS_threads
	Int CLEAN_BAMS_mem
        String sampleId
	String singularityContainerString
        File script

	command {
            ################# STEP 1 ################
            ##### Take mapped reads, remove duplicates and map to contaminant
            # Remove duplicate reads, and output paired-end fq files
            # Map fq files to contaminant genome
	    echo "Remapping mapped reads to ${contaminantRef['index']} ...";
            ${singularityContainerString} samtools view -u -F 1028 ${sampleBamFile} | \
            ${singularityContainerString} samtools sort -@ ${CLEAN_BAMS_threads} -n - | \
            ${singularityContainerString} samtools fastq -0 /dev/null - -s /dev/null -n | \
            ${singularityContainerString} bwa mem ${contaminantRef['index']} -p - -t ${CLEAN_BAMS_threads} | \
            ${singularityContainerString} samtools view -h -uS -@ ${CLEAN_BAMS_threads} - | \
            ${singularityContainerString} samtools sort -n -@ ${CLEAN_BAMS_threads} - | \
            ${singularityContainerString} samtools view -@ ${CLEAN_BAMS_threads} -o ${sampleId}.${refBase}.remapped.sam; \
            
            ################# STEP 2 ################
            ##### Compare matches in cigar strings of reads that mapped to contaminant and pathogen
            ${singularityContainerString} samtools view -u -F 1028 ${sampleBamFile} | \
            ${singularityContainerString} samtools sort -@ ${CLEAN_BAMS_threads} -n - | \
            ${singularityContainerString} samtools view -@ ${CLEAN_BAMS_threads} - | \
            ${singularityContainerString} python ${script} -1 - -2 ${sampleId}.${refBase}.remapped.sam > ${sampleId}.${refBase}.contaminant.sam 2> ${sampleId}.${refBase}.pathogen.sam; \
            rm -f ${sampleId}.${refBase}.remapped.sam; \
            # Unmerged bam files stay for some reason but I have checked that it does not affect the results
            rm -f samtools.*.bam; \

            ################# STEP 3 #################
            ##### Take the list of reads from *.pathogen.sam, remove them from the original bam file, and calculate read/mapping statistics
            cut -f1 ${sampleId}.${refBase}.pathogen.sam | sort | uniq > ${sampleId}.${refBase}.contaminant.read.ids; \
            rm -f ${sampleId}.${refBase}.pathogen.sam ${sampleId}.${refBase}.contaminant.sam;
	}
	runtime {
            runtime_minutes: '${CLEAN_BAMS_minutes}'
            cpus: '${CLEAN_BAMS_threads}'
            mem: '${CLEAN_BAMS_mem}'
        }
	output {
	    File contaminantReadIds = "${sampleId}.${refBase}.contaminant.read.ids"
        }
        meta {
                author: "Liina Kinkar"
                email: "liina.kinkar@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}

task createCleanedBams_task {
        File contaminantReadIds
        File sampleBamFile
	Int CREATE_CLEAN_BAMS_minutes
	Int CREATE_CLEAN_BAMS_threads
	Int CREATE_CLEAN_BAMS_mem
        String sampleId
	String singularityContainerString
	String baseBam = basename(sampleBamFile, ".bam")

	command {
            ################# STEP 3 #################
            ##### Take the list of reads from *.pathogen.sam, remove them from the original bam file, and calculate read/mapping statistics
            #cat ${sep=',' contaminantReadIds} | sort | uniq > contaminantReads.ids; \
            cat ${contaminantReadIds} | sort | uniq > contaminantReads.ids; \
            ${singularityContainerString} samtools view -@ ${CREATE_CLEAN_BAMS_threads} -h ${sampleBamFile} | fgrep -v -w -f contaminantReads.ids | ${singularityContainerString} samtools view -@ ${CREATE_CLEAN_BAMS_threads} -bS -h - -o ${baseBam}.cleaned.bam; \
            ${singularityContainerString} samtools index ${baseBam}.cleaned.bam; \
            ${singularityContainerString} samtools stats ${baseBam}.cleaned.bam > ${baseBam}.cleaned.bam.stats.txt; \
            rm -f contaminantReadIds;
	}
	runtime {
            runtime_minutes: '${CREATE_CLEAN_BAMS_minutes}'
            cpus: '${CREATE_CLEAN_BAMS_threads}'
            mem: '${CREATE_CLEAN_BAMS_mem}'
        }
	output {
      	    File cleanedBam = "${baseBam}.cleaned.bam"
            File cleanedBai = "${baseBam}.cleaned.bam.bai"
	    File cleanedBamStats = "${baseBam}.cleaned.bam.stats.txt" 
        }
        meta {
                author: "Liina Kinkar"
                email: "liina.kinkar@unimelb.edu.au"
                author: "Pasi Korhonen"
                email: "pasi.korhonen@unimelb.edu.au"
                description: "<DESCRIPTION>"
        }
        parameter_meta {
                # Inputs:
                Input1: "itype:<TYPE>: <DESCRIPTION>"
                # Outputs:
                Output1: "otype:<TYPE>: <DESCRIPTION>"
        }
}
