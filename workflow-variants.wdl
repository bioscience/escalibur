#############################################################################
#############################################################################
##
## Escalibur - Population Genomic Analysis Pipeline
## overcomes the difficulties in variant calling and is able to explore key
## aspects centering the population genetics of helminths, including defining
## population structures, estimating reproductive modes and identifying loci
## under local adaptation.
##
## Software usage: Java
## Input: Fastq files
##
##############################################################################
##############################################################################


############################### Import sub workflows #########################################
#import "./sub_workflows/index_selected_workflow.wdl" as indexingSubWorkflow
import "./sub_workflows/pe_filtering_workflow.wdl" as peFilteringSubworkflow
import "./sub_workflows/mapping_workflow.wdl" as mappingSubWorkflow
import "./sub_workflows/processHistStats_workflow.wdl" as processHistogramSubWorkflow
import "./sub_workflows/qc_hapcal_workflow.wdl" as qcMdupSubWorkflow
import "./sub_workflows/combineGenoype_workflow.wdl" as combineSubWorkflow
import "./sub_workflows/selectFilterZip_workflow.wdl" as selectVariantSNPSubWorkflow
import "./sub_workflows/independant_variant_calling_workflow.wdl" as indiviualVarCallingSubWorkflow
import "./sub_workflows/variant_filtering_workflow.wdl" as variantFilterSubWorkflow

######################### Import tasks #######################################################
#import "./tasks/read_report_task.wdl" as selectBestDb
#import "./tasks/create_best_db_map.wdl" as bestDbMap
#import "./tasks/filterBestRefGenomeBamFiles_task.wdl" as filterBestDb_task
#import "./tasks/bamMerge.wdl" as bamMerge_task
import "./tasks/readFiles_task.wdl" as readFiles
import "./tasks/index_task.wdl" as indexingTask

workflow variants_workflow {
	meta {
		author: "Noel Faux"
		email: "nfaux@unimelb.edu.au"
		author: "Bobbie Shaban"
		email: "babak.shaban@unimelb.edu.au"
		description: "<DESCRIPTION>"
	}
	parameter_meta {
		# Inputs:
		Input1: "itype:<TYPE>: <DESCRIPTION>"
		# Outputs:
		Output1: "otype:<TYPE>: <DESCRIPTION>"
	}

	File buildChrScript
	File maxDPscript
	File genVarReportScript
	Float ldCutOff
	Int ploidy
	String SNP_filt_exp
	String INDEL_filt_exp
	String call_type
	String picard
	String? singularityContainerPath
	String? singularityBindPath
	String? singularityContainerString = "singularity run -B " + singularityBindPath + " " + singularityContainerPath + ""
	Int maxIndelSize
	Int scafNumLim
	Int scafLenCutOff
	Int scafNumCo
	Int ldWinSize
	Int ldWinStep

	# input for fastq sample files to be processed
	File inputSampleFile # tsv of input files
	Array[Array[String]] arrayOfInputSamples = read_tsv(inputSampleFile)
	# Get number of samples
	Int nSamples = length(arrayOfInputSamples)

	# input for reference fasta files to be processed
	File selectedRefFile
	String selectedRefLabel # label for the selected reference file

	################# Index the selected reference genome ########################
        call indexingTask.indexing_ref_task as ref_index {
                input:
	            RefFastaFile = selectedRefFile,
	            singularityContainerString = singularityContainerString,
	            picardString = picard
        }
	
	Map[String, File] bestDbMap = ref_index.indexFiles
	#Map[String, File] bestDbMap = {"refFasta": selectedRefFile, "refFastaIndex": index_sel_sub_workflow.samIndex, "refDict": index_sel_sub_workflow.picardIndex }
	#Map[String, File] bestDbMap = {"refFasta": selectedRefFile, "refFastaIndex": selectedRefFileIndex, "refDict": selectedRefFileDict }
        
	#call readFiles.createMergedBamFileIds as bamFiles {
	#	input:
	#		singularityContainerString = singularityContainerString,
        #                singularityBindPath = singularityBindPath,
	#		readsFile = inputSampleFile,
        #                refLabel = selectedRefLabel,
        #                script = mergedBamFileIds
	#}

	#scatter (bamFile in bamFiles.mergedBamFiles) {
	scatter (bamFile in arrayOfInputSamples) {
		call qcMdupSubWorkflow.qc_sidx_hc_workflow {
			input:
				singularityContainerString = singularityContainerString,
				picardString = picard,
                                #base = bamFile,
                                inputBam = bamFile[1],
				picardString = picard,
				bestDbMap = bestDbMap,
				bestDb = selectedRefLabel,
				ploidy = ploidy
		}
	}

	################# Which call Type ########################
	# Independent / Individual
	if(call_type == "independent"){
		scatter (sampleMap in qc_sidx_hc_workflow.mDupSortedBamAndGVCFMap) {
			call indiviualVarCallingSubWorkflow.independant_variant_calling_wf as indepVarCall_wf {
				input :
					bestDb = selectedRefLabel,
					refGenomeDbMap = bestDbMap,
					sampleMap = sampleMap,
					picardString = picard,
					ploidy = ploidy,
					SNP_filt_exp = SNP_filt_exp,
					INDEL_filt_exp = INDEL_filt_exp,
					singularityContainerPath = singularityContainerPath,
					singularityContainerString = singularityContainerString
			}
		}
	}

	################# Combine GVCF  subworkflow ########################
	## Will later add option to run fast, joint or non-joint implementation

	Array[File?]? vcfSamples = if call_type == "independent" then indepVarCall_wf.RecalHapCHOutFile else qc_sidx_hc_workflow.haplo_GVCF

	##take in input file and sort samples into arrays
	#call sortInputfile_task {
        #            referenceArray = arrayOfReferenceFiles
        #            Write perl script to sort samples into arrays which are written into files
        #            Create output array
        #            Capture text files with glob
        #            Use glob for scatter
	#}

	## merge each of the arrays that are output from above in a scatter
	#scatter (sample in globArray) {
	#	call bamMerge_task.bamMerge_task {
	#		input:
	#		arrayOfEachSample = arrayOfEachSample,
	#	    	singularityContainerString = singularityContainerString,
	#		sampleName = sample.sampleName
	#	}
	#}	

	call combineSubWorkflow.combineGenotype_workflow {
		input :
			bestDbMap = bestDbMap,
			ploidy = ploidy,
			singularityContainerString = singularityContainerString,
			# Need to update to switch based on call_type
			vcfSamples = vcfSamples
	}

	 ################# Variantfiltering  subworkflow ########################
	 # change filter expressions to be in json file. Add Type (snp/indel) to json as well
	 call variantFilterSubWorkflow.snp_indel_var_filtering_workflow {
	 	input :
			bestDbMap = bestDbMap,
			singularityContainerPath = singularityContainerPath,
			singularityContainerString = singularityContainerString,
			buildChrScript = buildChrScript,
			genVarReportScript = genVarReportScript,
			maxDPscript = maxDPscript,
			maxIndelSize = maxIndelSize,
			genoTypeOutput = combineGenotype_workflow.genoTypeOutput,
			nSamples = nSamples,
			scfNumLim = scafNumLim,
			scafLenCutOff = scafLenCutOff,
			scfNumCo = scafNumCo,
			ldWinSize = ldWinSize,
			ldWinStep = ldWinStep,
			ldCutOff = ldCutOff
	}

	############  capture data filtering output ###############
	# capture data filtering output
        output {
		################## QC MarkDup - Fast Implementation ############
		#### Possibly change into map instead of 3 arrays #############
		Array[File?] hapGVCFarray = qc_sidx_hc_workflow.haplo_GVCF

		######################## Independant variant calling if done #########
		Array[File]? independantIntialRecalTables = indepVarCall_wf.Initial_RecaibratedSampleTable
		Array[File]? independantApplyRecalSampleTables = indepVarCall_wf.AfterApply_RecaibratedSampleTable
		Array[File]? independantInitialRecalBSQLbamFiles = indepVarCall_wf.sampleInitialRecal_BSQL_bam_file
		Array[File]? independantSecondRecalBSQLbamFiles = indepVarCall_wf.sampleSecondRecal_BSQL_bam_file
		Array[File]? independantRecalPlotsFiles = indepVarCall_wf.RecalPlots_File
		Array[File]? independantRecalCSVFiles = indepVarCall_wf.RecalCSV_File
		Array[File]? independantRecalHapCallsFiles = indepVarCall_wf.RecalHapCHOutFile

		######################## Joint variant calling if done ##################
		#						 			TODO							    #
		# Need to populate once the joint variant calling has been impelemented #
		#########################################################################
		
		##Capture output of merged bam files
		#Array[File] mergedBamFiles = bamMergeTask.mergedBamArray

		######################## Genotyping output ###########################
		File genotypeOutput = combineGenotype_workflow.genoTypeOutput

		######################## Variant filtration output ########################
		#File initialFilteredSnps = snp_indel_var_filtering_workflow.outInitialFilteredSNPs
		#File highFedalitySNPs = snp_indel_var_filtering_workflow.outHighFedalitySNPs
		#File indels = snp_indel_var_filtering_workflow.outINDELs
		File? finalVarReport = snp_indel_var_filtering_workflow.outFinalVarReport
	}
}
